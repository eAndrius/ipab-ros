## Core Utilities
-----------------

This workspace contains certain core functionality which is independent of the application. It should be used only for such things as generic libraries and common tools.
