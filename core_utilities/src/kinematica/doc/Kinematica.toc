\contentsline {section}{\numberline {1}Introduction}{3}
\contentsline {subsection}{\numberline {1.1}The Kinematica Library}{3}
\contentsline {subsection}{\numberline {1.2}Structure of the Document}{3}
\contentsline {section}{\numberline {2}Kinematics Theory}{4}
\contentsline {subsection}{\numberline {2.1}Robot Structure}{4}
\contentsline {subsection}{\numberline {2.2}Redefining the Root Frame}{5}
\contentsline {subsection}{\numberline {2.3}Computing Forward Kinematics}{5}
\contentsline {subsection}{\numberline {2.4}Computing Jacobians}{5}
\contentsline {section}{\numberline {3}Using the Library}{7}
\contentsline {subsection}{\numberline {3.1}Setup}{7}
\contentsline {subsection}{\numberline {3.2}Public API}{8}
\contentsline {section}{\numberline {4}The Kinematica Implementation}{13}
\contentsline {subsection}{\numberline {4.1}Major Design Decisions}{13}
\contentsline {subsection}{\numberline {4.2}Data Types and Auxiliaries}{13}
\contentsline {subsection}{\numberline {4.3}Class Attributes}{16}
\contentsline {subsection}{\numberline {4.4}Initialisers}{16}
\contentsline {subsection}{\numberline {4.5}Updaters}{21}
\contentsline {subsection}{\numberline {4.6}Accessors}{21}
\contentsline {section}{\numberline {A}Known Limitations}{23}
\contentsline {section}{\numberline {B}Version History}{23}
