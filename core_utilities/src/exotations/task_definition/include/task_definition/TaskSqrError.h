/*!*******************************************************************!*\
|    TaskSqrError provides the base functionality for creating squared error-  |
|  functions in EXOTica. It is thread-safe.                             |
|                                                                       |
|           Developer: Michael Camilleri (mcamnadur@gmail.com)          |
|                    Last Edited: 17 - March - 2014                     |
\***********************************************************************/

#ifndef EXOTICA_ERROR_CLASS_H
#define EXOTICA_ERROR_CLASS_H

#include "exotica/TaskDefinition.h"//!< The Component base
#include "exotica/Factory.h"      //!< The Factory template
#include "exotica/Tools.h"        //!< For XML-Parsing/ErrorFunction definition
#include "exotica/Test.h"         //!< For Testing factory
#include <Eigen/Dense>            //!< Generally dense manipulations should be enough
#include <boost/thread/mutex.hpp> //!< The boost thread-library for synchronisation
 
namespace exotica
{
  class TaskSqrError : public TaskDefinition
  {
    public:
      /**
       * \brief Default Constructor
       */
      TaskSqrError();
      
      /**
       * \brief Setter for a new goal condition
       * @param y_star[in]  The Goal vector (in task-space co-ordinates)
       * @return            Currently returns SUCCESS always
       */
      EReturn setGoal(const Eigen::VectorXd & y_star);
      
      /**
       * \brief This variant returns the Error-Vector per dimension
       *        It SHOULD NOT make use of the Weighting matrix, but pass this out as part of the parameters. This is intended to be used with LQR-type solvers, and its sole use is to 
       *        introduce any non-linear warpings in the error before the weighting matrix.
       * @pre              Should request task-space vector, check for correct size etc...
       * @param error[out] The error (y* - y)
       * @param W    [out] The Weighting Matrix: its dimensionality should be the same as the error: should incorporate the rho inter-task weighting
       * @return           Indication of success/failure:
       */
      EReturn getErrorVector(Eigen::VectorXd & error, Eigen::MatrixXd & W);
      
      /**
       * \brief This variant returns the proper scalar Error Value
       *        This is intended for iterative-type solvers or where the Error is highly non-linear. It is in fact the true definition of an Error Function
       * @pre              Should request task-space vector, check for correct size etc...
       * @param error[out] The error f(y*, y, W)
       * @return           Indication of success/failure:
       */
      EReturn getErrorScalar(double & error);
      
    protected:
      /**
       * \brief Getter for the goal-value (to be used by the derived classes)
       * @param y_star[out] The goal vector
       * @return            Indication of success: SUCCESS if defined, MMB_NIN if not initialised correctly
       */
      EReturn getGoal(Eigen::VectorXd & y_star);
      
      /**
       * \brief Getter for the weighting matrix (solver might require access to them)
       * @param W[out] The placeholder for the weight matrix
       * @return       SUCCESS if initialised correctly, MMB_NIN if not
       */
      EReturn getWeights(Eigen::MatrixXd & W);
      
      /**
       * \brief Getter for the rho value
       * @param rho   The inter-task weight
       * @return      SUCCESS if ok, MMB_NIN if not initialised
       */
      EReturn getRho(double & rho);
      
      /**
       * \brief Setter for the weighting matrix
       * @param W[in] The weighting matrix
       * @return      Always successful
       */
      EReturn setWeights(const Eigen::MatrixXd & W);
      
      /**
       * \brief Setter for the rho weight
       * @param rho[in] The inter-task weight
       * @return        Always successful
       */
      EReturn setRho(const double & rho);
      
      /**
       * \brief Concrete implementation of the initDerived
       * @param handle  The handle to the XML-element describing the ErrorFunction Function
       * @return        Should indicate success/failure
       */
      virtual EReturn initDerived(tinyxml2::XMLHandle & handle);
      
    private:
      /** The internal storage **/
      Eigen::VectorXd   y_star_;  //!< The goal vector
      Eigen::MatrixXd   W_;       //!< The Weight matrix
      double            rho_;     //!< The scalar inter-task weight
      boost::mutex      y_lock_;  //!< Locking mechanism for the goal vector
      boost::mutex      W_lock_;  //!< Locking mechanism for the weight matrix
      boost::mutex      rho_lock_;//!< Locking mechanism for rho
  };
}
#endif
