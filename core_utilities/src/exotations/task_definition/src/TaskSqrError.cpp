#include "task_definition/TaskSqrError.h"

REGISTER_TASKDEFINITION_TYPE("TaskSqrError", exotica::TaskSqrError);

exotica::TaskSqrError::TaskSqrError()
{
  rho_ = nan(""); //! Invalid value
}

exotica::EReturn exotica::TaskSqrError::initDerived(tinyxml2::XMLHandle & handle)
{
  //!< Temporaries
  Eigen::MatrixXd W;      //!< The weighting matrix
  Eigen::VectorXd y_star; //!< The Goal vector
  double          rho;
  EReturn temp_return = SUCCESS;

  //!< First get the weight matrix, W which is compulsary
  if (!handle.FirstChildElement("Weights").ToElement()) { temp_return = PAR_ERR; }
  if (temp_return) { INDICATE_FAILURE; }
  else             {temp_return = getMatrix(*(handle.FirstChildElement("Weights").ToElement()), W); }
  if (temp_return) { INDICATE_FAILURE; }
  else             { temp_return = setWeights(W); }
  
  //!< Now also get the rho, which is also compulsary
  if (temp_return) { INDICATE_FAILURE; }
  else             { if (!handle.FirstChildElement("Rho").ToElement()) {temp_return = PAR_ERR; } }
  if (temp_return) { INDICATE_FAILURE; }
  else             { temp_return = getDouble(*(handle.FirstChildElement("Rho").ToElement()), rho); }
  if (temp_return) { INDICATE_FAILURE; }
  else             { temp_return = setRho(rho); }
   
  //!< Check if goal specified
  if (temp_return) { INDICATE_FAILURE; }
  else
  {
    if (handle.FirstChildElement("Goal").ToElement())
    {
      temp_return = getVector(*(handle.FirstChildElement("Goal").ToElement()), y_star);
      if (!temp_return) { temp_return = setGoal(y_star); }
      if (temp_return)  { temp_return = WARNING; }  //!< Warning: Goal is not necessary but failure in this case should be warned: otherwise it will be success
    }
    else
    {
      temp_return = WARNING;  //!< Warning that goal is not present
    }
  }
  
  //!< Decide on the return
  if (temp_return and temp_return != WARNING)
  {
    W       = Eigen::MatrixXd();
    y_star  = Eigen::VectorXd();
    rho     = nan("");
    setWeights(W);    //!< Clear everything
    setGoal(y_star);
    setRho(rho);
  }
  
  return temp_return;
}

exotica::EReturn exotica::TaskSqrError::getErrorVector(Eigen::VectorXd & error, Eigen::MatrixXd & W)
{
  exotica::EReturn  temp_return;
  Eigen::VectorXd   y_star, y;
  double            rho;
  
  //!< First resolve Error Vector
  temp_return = phi(y);  //!< First attempt to get the current task-space co-ordinates (using functionality from ProblemComponent)
  if (temp_return) { return temp_return; }
  temp_return = getGoal(y_star); //!< Next attempt to get the goal (using functionality from ErrorFunction)
  if (temp_return) { return temp_return; }
  if (y_star.size() != y.size()) { return exotica::MMB_NIN; }  //!< Check that sizes are correct
  error = y_star - y; //!< Note that the Weighting Matrix does not factor in here, since this is used by the solver in its analytical form
  
  //!< Now deal with the Weighting Matrix
  temp_return = getWeights(W);
  if (temp_return) { return temp_return; }
  temp_return = getRho(rho);
  if (temp_return) { return temp_return; }
  W = W*rho;
  
  return SUCCESS;
}

exotica::EReturn exotica::TaskSqrError::getErrorScalar(double & error)
{
  exotica::EReturn  temp_return;
  Eigen::VectorXd   y_star, y, err;
  Eigen::MatrixXd   W;
  
  temp_return = phi(y);  //!< First attempt to get the current task-space co-ordinates (using functionality from TaskDefinition)
  if (temp_return) { return temp_return; }
          
  temp_return = getGoal(y_star); //!< Next attempt to get the goal (using functionality from ErrorFunction)
  if (temp_return) { return temp_return; }

  if (y_star.size() != y.size()) { return exotica::MMB_NIN; }  //!< Check that sizes are correct
  
  temp_return = getWeights(W);
  if (temp_return) { return temp_return; }
  
  err = y_star - y;
  error = err.transpose() * W * err;  //!< The weighted norm
  
  return SUCCESS;
}

exotica::EReturn exotica::TaskSqrError::setGoal(const Eigen::VectorXd & y_star)
{
  LOCK(y_lock_);
  y_star_ = y_star;
  return SUCCESS;
}


exotica::EReturn exotica::TaskSqrError::getGoal(Eigen::VectorXd & y_star)
{
  LOCK(y_lock_);
  if (y_star_.size())
  {
    y_star = y_star_;
    return SUCCESS;
  }
  else
  {
    return MMB_NIN;
  }
}


exotica::EReturn exotica::TaskSqrError::setWeights(const Eigen::MatrixXd & W)
{
  LOCK(W_lock_);
  W_ = W;
  return SUCCESS;
}

exotica::EReturn exotica::TaskSqrError::setRho(const double & rho)
{
  LOCK(rho_lock_);
  rho_ = rho;
  return SUCCESS;
}


exotica::EReturn exotica::TaskSqrError::getWeights(Eigen::MatrixXd & W)
{
  LOCK(W_lock_);
  if (W_.size())
  {
    W = W_;
    return SUCCESS;
  }
  else
  {
    return MMB_NIN;
  }
}

exotica::EReturn exotica::TaskSqrError::getRho(double & rho)
{
  LOCK(rho_lock_);
  if (!isnan(rho_))
  {
    rho = rho_;
    return SUCCESS;
  }
  else
  {
    return MMB_NIN;
  }
}
