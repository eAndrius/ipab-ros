#ifndef EXOTICA_TASKMAP_EFF_POSITION_H
#define EXOTICA_TASKMAP_EFF_POSITION_H

#include <exotica/TaskMap.h>
#include <exotica/Factory.h>
#include <exotica/Test.h>
#include <tinyxml2/tinyxml2.h>
#include <kinematica/KinematicTree.h>
#include <Eigen/Dense>
#include <boost/thread/mutex.hpp>

namespace exotica //!< Since this is part of the core library, it will be within the same namespace
{
  class EffPosition : public TaskMap
  {
    public:
      /**
       * \brief Default constructor
       */
      EffPosition();
      
      /**
       * \brief Concrete implementation of the update method
       */
      virtual EReturn update(const Eigen::VectorXd & x);
      
      /**
       * \brief Initialiser (manual)
       * @param urdf_file     The file describing the robot structure
       * @param solution_form The Kinematica parameters
       */
      EReturn initialise(const std::string & urdf_file, const kinematica::SolutionForm_t & solution_form);
      
      /**
       * \overload
       * @param urdf_tree     Alternatively, the KDL Tree itself...
       * @param solution_form The Kinematica parameters
       */
      EReturn initialise(const KDL::Tree   & urdf_tree, const kinematica::SolutionForm_t & solution_form);
      
    protected:
      /**
       * \brief Concrete implementation of the initialisation method
       */
      virtual EReturn initDerived(tinyxml2::XMLHandle & handle);
      
    private:
      /******* Member Variables *******/
      kinematica::KinematicTree   robot_solver_;   //!< The actual FK Robot solver
      boost::mutex                solver_lock_;    //!< For thread synchronisation
      bool                        initialised_;    //!< For Error checking
  };
}

#endif
