/*
 * IMesh.h
 *
 *  Created on: 18 Mar 2014
 *      Author: yimingyang
 */

#ifndef IMESH_H_
#define IMESH_H_

#include <exotica/TaskMap.h>
#include <exotica/Factory.h>
#include <exotica/Test.h>
#include <tinyxml2/tinyxml2.h>
#include <kinematica/KinematicTree.h>
#include <Eigen/Eigen>
#include <boost/thread/mutex.hpp>

namespace exotica
{
	/**
	 * @brief	Implementation of Interaction Mesh Task Map
	 */
	class IMesh: public TaskMap
	{
		public:
			/**
			 * @brief	Default constructor
			 */
			IMesh();

			/**
			 * @brief	Destructor
			 */
			virtual ~IMesh();

			/**
			 * @brief	Concrete implementation of update method
			 * @param	x	Joint space configuration
			 */
			virtual EReturn update(const Eigen::VectorXd & x);

			/**
			 * @brief	Set edge weight(s)
			 * @param	i,j		Vertices i, j
			 * @param	weight	Edge weight
			 * @param	weights	Weight matrix
			 * @return	Exotica return type
			 */
			EReturn setWeight(int i, int j, double weight);
			EReturn setWeights(const Eigen::MatrixXd & weights);

			/**
			 * @brief	Get Laplace coordinates
			 * @return	Laplace Matrix or Laplace Verctor
			 */
			Eigen::Matrix3Xd getLaplace();
			Eigen::VectorXd getVectorLaplace();
		protected:
			/**
			 * @brief	Concrete implementation of initialisation from xml
			 * @param	handle	XML handler
			 */
			virtual EReturn initDerived(tinyxml2::XMLHandle & handle);
		private:
			/** Member Functions **/

			/**
			 * @brief	Compute laplace coordinates of a vertices set
			 * @param	V		3xN matrix of vertices positions
			 * @param	wsum	Array of weight normalisers (out put)
			 * @param	dist	Triangular matrix of distances between vertices (out put)
			 * @return	3xN Laplace coordinates
			 */
			Eigen::Matrix3Xd computeLaplace(const Eigen::Matrix3Xd & V);
			Eigen::Matrix3Xd computeLaplace(const Eigen::Matrix3Xd & V, Eigen::VectorXd & wsum,
					Eigen::MatrixXd & dist);

			/**
			 * @brief	Compute Jacobian of the laplace coordinates with respect to the joint angle space
			 * @param	q	Joint angles
			 * @return	Jacobian matrix
			 */
			Eigen::MatrixXd computeJacobian(const Eigen::VectorXd & q);

			/**
			 * @brief	Update newest vertices status
			 * @param	q	Robot joint configuration
			 * @return	Exotica return type
			 */
			EReturn updateVertices(const Eigen::VectorXd & q);
			/** Member Variables **/
			kinematica::KinematicTree robot_solver_;	//!< Robot solver
			boost::mutex locker_;	//!<Thread locker
			bool initialised_;	//!< Initialisation flag

			/** Interaction Mesh Variables **/
			Eigen::Matrix3Xd vertices_;	//!< Vertex positions
			Eigen::Matrix3Xd laplace_;	//!< Laplace coordinates
			Eigen::MatrixXd weights_;	//!< Weighting matrix, currently set to ones
	};
}

#endif /* IMESH_H_ */
