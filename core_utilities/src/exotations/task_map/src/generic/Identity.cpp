#include "generic/Identity.h"

REGISTER_TASKMAP_TYPE("Identity", exotica::Identity);
REGISTER_FOR_XML_TEST("Identity", "Identity.xml");

exotica::Identity::Identity()
{
  //!< Empty constructor
}

exotica::EReturn exotica::Identity::update(const Eigen::VectorXd & x)
{
  invalidate();
  if (setPhi(x) == SUCCESS)
  {
    return setJacobian(Eigen::MatrixXd::Identity(x.size(), x.size()));
  }
  else
  {
    return FAILURE;
  }
}

exotica::EReturn exotica::Identity::initDerived(tinyxml2::XMLHandle & handle)
{
  return SUCCESS;
}
