#include "kinematic_maps/EffPosition.h"

REGISTER_TASKMAP_TYPE("EffPosition", exotica::EffPosition);
REGISTER_FOR_XML_TEST("EffPosition", "EffPosition.xml");

exotica::EffPosition::EffPosition()
{
  initialised_ = false;
}

exotica::EReturn exotica::EffPosition::update(const Eigen::VectorXd & x)
{
  LOCK(solver_lock_);
  
  invalidate();
  if (!initialised_) { return MMB_NIN; }
    
  //!< Temporaries
  bool            success = true;
  exotica::EReturn temp_return = FAILURE;
  Eigen::VectorXd phi;
  Eigen::MatrixXd jac;
  
  success = robot_solver_.updateConfiguration(x); //!< Attempt to update the FK
  
  if (success) { success = robot_solver_.generateForwardMap(phi); } //!< Attempt to generate phi
  
  if (success) { success = robot_solver_.generateJacobian(jac); }   //!< Attempt to generate jacobian
  
  if (success) { temp_return = setPhi(phi); }
  
  if (temp_return == SUCCESS) { temp_return = setJacobian(jac); }
  
  return temp_return;
}


exotica::EReturn exotica::EffPosition::initialise(const std::string & urdf_file, const kinematica::SolutionForm_t & solution_form)
{
  LOCK(solver_lock_);
  
  initialised_ = robot_solver_.initKinematics(urdf_file, solution_form);
  
  return initialised_ ? SUCCESS : FAILURE;
}


exotica::EReturn exotica::EffPosition::initialise(const KDL::Tree   & urdf_tree, const kinematica::SolutionForm_t & solution_form)
{
  LOCK(solver_lock_);
  
  initialised_ = robot_solver_.initKinematics(urdf_tree, solution_form);
  
  return initialised_ ? SUCCESS : FAILURE;
}


exotica::EReturn exotica::EffPosition::initDerived(tinyxml2::XMLHandle & handle)
{
  std::string                 urdf_file;
  kinematica::SolutionForm_t  solution;
  
  //!< Checks for compulsaries
  if (!handle.FirstChildElement("Urdf").ToElement()) { INDICATE_FAILURE; return PAR_ERR; } //!< We must have a urdf specification at the very least
  if (!handle.FirstChildElement("Update").ToElement()) { INDICATE_FAILURE; return PAR_ERR; } //!< We must have the list of joints
  
  //!< First the file
  urdf_file = handle.FirstChildElement("Urdf").ToElement()->GetText();
  if (urdf_file.empty()) { return PAR_ERR; }
  
  //!< Now the solution params:
  solution.root_segment = "";
  solution.root_seg_off = KDL::Frame::Identity();
  if (handle.FirstChildElement("Root").ToElement())
  {
    if (!handle.FirstChildElement("Root").ToElement()->Attribute("segment")) { INDICATE_FAILURE; return PAR_ERR; }
    solution.root_segment = handle.FirstChildElement("Root").ToElement()->Attribute("segment");
    
    if (handle.FirstChildElement("Root").FirstChildElement("vector").ToElement())
    {
      Eigen::VectorXd temp_vector;
      if (exotica::getVector(*(handle.FirstChildElement("Root").FirstChildElement("vector").ToElement()), temp_vector) != SUCCESS) { INDICATE_FAILURE; return PAR_ERR; }
      if (temp_vector.size() != 3)  { INDICATE_FAILURE; return PAR_ERR; }
      solution.root_seg_off.p.x(temp_vector(0)); solution.root_seg_off.p.y(temp_vector(1)); solution.root_seg_off.p.z(temp_vector(2));
    }
    
    if (handle.FirstChildElement("Root").FirstChildElement("quaternion").ToElement())
    {
      Eigen::VectorXd temp_vector;
      if (exotica::getVector(*(handle.FirstChildElement("Root").FirstChildElement("quaternion").ToElement()), temp_vector) != SUCCESS) { INDICATE_FAILURE; return PAR_ERR; }
      if (temp_vector.size() != 4)  { INDICATE_FAILURE; return PAR_ERR; }
      solution.root_seg_off.M = KDL::Rotation::Quaternion(temp_vector(1), temp_vector(2), temp_vector(3), temp_vector(0));
    } 
  }
  
  solution.zero_other_joints = true;  //!< By default it is true
  if (handle.FirstChildElement("Update").ToElement()->Attribute("zero_unnamed"))  //!< If it exists
  {
    if (handle.FirstChildElement("Update").ToElement()->QueryBoolAttribute("zero_unnamed", &solution.zero_other_joints) != tinyxml2::XML_NO_ERROR) { INDICATE_FAILURE; return PAR_ERR; }  //!< If exists but wrongly defined
  }
  tinyxml2::XMLHandle joint_handle(handle.FirstChildElement("Update").FirstChildElement("joint"));
  while (joint_handle.ToElement())
  {
    if (!joint_handle.ToElement()->Attribute("name")) {INDICATE_FAILURE; return PAR_ERR; } //!< If no name exists
    solution.joints_update.push_back(joint_handle.ToElement()->Attribute("name"));
    joint_handle = joint_handle.NextSiblingElement("joint");
  }
  if (solution.joints_update.size() < 1) {INDICATE_FAILURE; return PAR_ERR; }  //!< If no joints specified
  
  solution.ignore_unused_segs = true;
  if (handle.FirstChildElement("EndEffector").ToElement()->Attribute("ignore_unused"))
  {
    if (handle.FirstChildElement("EndEffector").ToElement()->QueryBoolAttribute("ignore_unused", &solution.ignore_unused_segs) != tinyxml2::XML_NO_ERROR) {INDICATE_FAILURE; return PAR_ERR; }
  }
  tinyxml2::XMLHandle segment_handle(handle.FirstChildElement("EndEffector").FirstChildElement("limb"));
  while (segment_handle.ToElement())
  {
    if (!segment_handle.ToElement()->Attribute("segment")) { INDICATE_FAILURE; return PAR_ERR; }
    solution.end_effector_segs.push_back(segment_handle.ToElement()->Attribute("segment"));
    KDL::Frame temp_frame = KDL::Frame::Identity(); //!< Initialise to identity
    if (segment_handle.FirstChildElement("vector").ToElement())
    {
      Eigen::VectorXd temp_vector;
      if (exotica::getVector(*(segment_handle.FirstChildElement("vector").ToElement()), temp_vector) != SUCCESS) { INDICATE_FAILURE; return PAR_ERR; }
      if (temp_vector.size() != 3)  { INDICATE_FAILURE; return PAR_ERR; }
      temp_frame.p.x(temp_vector(0)); temp_frame.p.y(temp_vector(1)); temp_frame.p.z(temp_vector(2));
    }
    if (segment_handle.FirstChildElement("quaternion").ToElement())
    {
      Eigen::VectorXd temp_vector;
      if (exotica::getVector(*(segment_handle.FirstChildElement("quaternion").ToElement()), temp_vector) != SUCCESS) { INDICATE_FAILURE; return PAR_ERR; }
      if (temp_vector.size() != 4)  { INDICATE_FAILURE; return PAR_ERR; }
      temp_frame.M = KDL::Rotation::Quaternion(temp_vector(1), temp_vector(2), temp_vector(3), temp_vector(0));
    }
    solution.end_effector_offs.push_back(temp_frame);
    segment_handle = segment_handle.NextSiblingElement("limb");
  }
   
  return initialise(urdf_file, solution); //!< Attempt to initialise

}
