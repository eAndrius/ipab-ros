/*
 * IMesh.cpp
 *
 *  Created on: 18 Mar 2014
 *      Author: yimingyang
 */

#include "kinematic_maps/IMesh.h"

exotica::IMesh::IMesh()
{
	initialised_ = false;
}

exotica::IMesh::~IMesh()
{
	//TODO
}

exotica::EReturn exotica::IMesh::update(const Eigen::VectorXd & x)
{
	LOCK(locker_);
	refresh();
	if (!initialised_)
	{
		return MMB_NIN;
	}
	bool success = false;
	return SUCCESS;
}

exotica::EReturn exotica::IMesh::initDerived(tinyxml2::XMLHandle & handle)
{
	std::string urdf;
	kinematica::SolutionForm_t solution;

	//Checks for compulsaries
	if (!handle.FirstChildElement("Urdf").ToElement())
	{
		INDICATE_FAILURE;
		return PAR_ERR;
	}
	if (!handle.FirstChildElement("Update").ToElement())
	{
		INDICATE_FAILURE;
		return PAR_ERR;
	}
	urdf = handle.FirstChildElement("Urdf").ToElement()->GetText();
	if (urdf.empty())
	{
		INDICATE_FAILURE;
		return PAR_ERR;
	}

	//Pass kinematica parameters
	solution.root_segment = "";
	solution.root_seg_off = KDL::Frame::Identity();
	if (handle.FirstChildElement("Root").ToElement())
	{
		if (!handle.FirstChildElement("Root").ToElement()->Attribute("segment"))
		{
			INDICATE_FAILURE;
			return PAR_ERR;
		}
		solution.root_segment = handle.FirstChildElement("Root").ToElement()->Attribute("segment");

		if (handle.FirstChildElement("Root").FirstChildElement("vector").ToElement())
		{
			Eigen::VectorXd temp_vector;
			if (exotica::getVector(*(handle.FirstChildElement("Root").FirstChildElement("vector").ToElement()), temp_vector)
					!= SUCCESS)
			{
				INDICATE_FAILURE;
				return PAR_ERR;
			}
			if (temp_vector.size() != 3)
			{
				INDICATE_FAILURE;
				return PAR_ERR;
			}
			solution.root_seg_off.p.x(temp_vector(0));
			solution.root_seg_off.p.y(temp_vector(1));
			solution.root_seg_off.p.z(temp_vector(2));
		}

		if (handle.FirstChildElement("Root").FirstChildElement("quaternion").ToElement())
		{
			Eigen::VectorXd temp_vector;
			if (exotica::getVector(*(handle.FirstChildElement("Root").FirstChildElement("quaternion").ToElement()), temp_vector)
					!= SUCCESS)
			{
				INDICATE_FAILURE;
				return PAR_ERR;
			}
			if (temp_vector.size() != 4)
			{
				INDICATE_FAILURE;
				return PAR_ERR;
			}
			solution.root_seg_off.M =
					KDL::Rotation::Quaternion(temp_vector(1), temp_vector(2), temp_vector(3), temp_vector(0));
		}
	}

	solution.zero_other_joints = true;  //!< By default it is true
	if (handle.FirstChildElement("Update").ToElement()->Attribute("zero_unnamed")) //!< If it exists
	{
		if (handle.FirstChildElement("Update").ToElement()->QueryBoolAttribute("zero_unnamed", &solution.zero_other_joints)
				!= tinyxml2::XML_NO_ERROR)
		{
			INDICATE_FAILURE;
			return PAR_ERR;
		}  //!< If exists but wrongly defined
	}
	tinyxml2::XMLHandle joint_handle(handle.FirstChildElement("Update").FirstChildElement("joint"));
	while (joint_handle.ToElement())
	{
		if (!joint_handle.ToElement()->Attribute("name"))
		{
			INDICATE_FAILURE;
			return PAR_ERR;
		} //!< If no name exists
		solution.joints_update.push_back(joint_handle.ToElement()->Attribute("name"));
		joint_handle = joint_handle.NextSiblingElement("joint");
	}
	if (solution.joints_update.size() < 1)
	{
		INDICATE_FAILURE;
		return PAR_ERR;
	}  //!< If no joints specified

	solution.ignore_unused_segs = true;
	if (handle.FirstChildElement("EndEffector").ToElement()->Attribute("ignore_unused"))
	{
		if (handle.FirstChildElement("EndEffector").ToElement()->QueryBoolAttribute("ignore_unused", &solution.ignore_unused_segs)
				!= tinyxml2::XML_NO_ERROR)
		{
			INDICATE_FAILURE;
			return PAR_ERR;
		}
	}
	tinyxml2::XMLHandle segment_handle(handle.FirstChildElement("EndEffector").FirstChildElement("limb"));
	while (segment_handle.ToElement())
	{
		if (!segment_handle.ToElement()->Attribute("segment"))
		{
			INDICATE_FAILURE;
			return PAR_ERR;
		}
		solution.end_effector_segs.push_back(segment_handle.ToElement()->Attribute("segment"));
		KDL::Frame temp_frame = KDL::Frame::Identity(); //!< Initialise to identity
		if (segment_handle.FirstChildElement("vector").ToElement())
		{
			Eigen::VectorXd temp_vector;
			if (exotica::getVector(*(segment_handle.FirstChildElement("vector").ToElement()), temp_vector)
					!= SUCCESS)
			{
				INDICATE_FAILURE;
				return PAR_ERR;
			}
			if (temp_vector.size() != 3)
			{
				INDICATE_FAILURE;
				return PAR_ERR;
			}
			temp_frame.p.x(temp_vector(0));
			temp_frame.p.y(temp_vector(1));
			temp_frame.p.z(temp_vector(2));
		}
		if (segment_handle.FirstChildElement("quaternion").ToElement())
		{
			Eigen::VectorXd temp_vector;
			if (exotica::getVector(*(segment_handle.FirstChildElement("quaternion").ToElement()), temp_vector)
					!= SUCCESS)
			{
				INDICATE_FAILURE;
				return PAR_ERR;
			}
			if (temp_vector.size() != 4)
			{
				INDICATE_FAILURE;
				return PAR_ERR;
			}
			temp_frame.M =
					KDL::Rotation::Quaternion(temp_vector(1), temp_vector(2), temp_vector(3), temp_vector(0));
		}
		solution.end_effector_offs.push_back(temp_frame);
		segment_handle = segment_handle.NextSiblingElement("limb");
	}
	initialised_ = robot_solver_.initKinematics(urdf, solution);
	return initialised_ ? SUCCESS : FAILURE;
}

Eigen::Matrix3Xd exotica::IMesh::computeLaplace(const Eigen::Matrix3Xd & V)
{
	uint N = V.cols();
	Eigen::MatrixXd dist(N, N);
	Eigen::VectorXd wsum(N);
	return computeLaplace(V, dist, wsum);
}

Eigen::Matrix3Xd exotica::IMesh::computeLaplace(const Eigen::Matrix3Xd & V, Eigen::VectorXd & wsum,
		Eigen::MatrixXd & dist)
{
	CHECK_EXECUTION;
	uint N = V.cols();
	Eigen::Matrix3Xd L(3, N);
	dist = Eigen::MatrixXd::Zero(N, N);
	wsum = Eigen::VectorXd::Zero(N);
	if (weights_.cols() < N)
		weights_ = Eigen::MatrixXd::Ones(N, N);

	uint j, l;
	double w;
	/** Compute distance matrix (inverse proportional) */
	for (j = 0; j < N; j++)
	{
		for (l = 0; l < N; l++)
		{
			dist(j, l) = sqrt((V.col(j) - V.col(l)).dot((V.col(j) - V.col(l))));

			//dist(j, l) = sqrt((V.col(j)).dot(V.col(l)));
		}
	}
	/** Computer weight normaliser */
	for (j = 0; j < N; j++)
	{
		for (l = 0; l < N; l++)
		{
			if (dist(j, l) > 0)
			{
				wsum(j) += weights_(j, l) / dist(j, l);
			}
		}
	}
	/** Compute Laplace coordinates */
	for (j = 0; j < N; j++)
	{
		L.col(j) = V.col(j);
		for (l = 0; l < N; l++)
		{
			if (j != l)
			{
				if (dist(j, l) > 0 && wsum(j) > 0)
				{
					w = weights_(j, l) / (dist(j, l) * wsum(j));
					L.col(j) -= V.col(l) * w;
				}
			}
		}
	}
#ifdef DEBUG_MODE
	std::cout << "Laplace Coordinates:\n" << L << std::endl;
#endif
	CHECK_EXECUTION;
	return L;
}

Eigen::Matrix3Xd exotica::IMesh::getLaplace()
{
	if (!initialised_)
	{
		INDICATE_FAILURE;
		return Eigen::Matrix3Xd(3, 0).Zero();
	}
	return laplace_;
}
Eigen::VectorXd exotica::IMesh::getVectorLaplace()
{
	if (!initialised_)
	{
		INDICATE_FAILURE;
		return Eigen::VectorXd(0).Zero();
	}
	Eigen::VectorXd lap_vec(3 * laplace_.cols());
	for (int i = 0; i < laplace_.cols(); i++)
	{
		lap_vec(3 * i) = laplace_(0, i);
		lap_vec(3 * i + 1) = laplace_(1, i);
		lap_vec(3 * i + 2) = laplace_(2, i);
	}
	return lap_vec;
}

Eigen::MatrixXd exotica::IMesh::computeJacobian(const Eigen::VectorXd & q)
{
	updateVertices(q);
	uint N = q.size();
	uint M = vertices_.cols();
#ifdef DEBUG_MODE
	std::cout << "Computing Jacobian M = " << M << ", N =" << N << std::endl;
#endif
	Eigen::MatrixXd dist(M, M);
	Eigen::VectorXd wsum(M);
	laplace_ = computeLaplace(vertices_, wsum, dist);
	setPhi(getVectorLaplace(), 0);
#ifdef DEBUG_MODE
	std::cout << "wsum:\n" << wsum << std::endl;
	std::cout << "dist:\n" << dist << std::endl;
	std::cout << "dist sum: " << dist.sum() << std::endl;
#endif

	Eigen::Matrix3Xd & p = vertices_;
	Eigen::MatrixXd _p(3 * M, N);
	Eigen::MatrixXd J(3 * M, N);
	Eigen::MatrixXd joint_jac;

	_p.setZero();
	kinematic_solver_.generateJacobian(joint_jac);
#ifdef DEBUG_MODE
	std::cout << "Joint Jac:" << joint_jac.cols() << "x" << joint_jac.rows() << " \n" << joint_jac << std::endl;
#endif
	_p.block(0, 0, joint_jac.rows(), joint_jac.cols()) = joint_jac;
#ifdef DEBUG_MODE
	std::cout << "_p: \n" << _p << std::endl;
#endif
	double A, Sk, Sl, w, _w;
	uint i, j, k, l;

	for (i = 0; i < N; i++)
	{
		for (j = 0; j < M; j++)
		{
			J.block<3, 1>(3 * j, i) = _p.block<3, 1>(3 * j, i);
			for (l = 0; l < M; l++)
			{
				if (j != l)
				{
					if (dist(j, l) > 0 && wsum(j) > 0)
					{
						w = weights_(j, l) / (dist(j, l) * wsum(j));
						A = 0;
						Sl = ((p.col(j) - p.col(l)).dot((_p.block<3, 1>(3 * j, i)
								- _p.block<3, 1>(3 * l, i)))) / dist(j, l);
						for (k = 0; k < M; k++)
						{
							if (j != k && dist(j, k) > 0)
							{
								Sk = ((p.col(j) - p.col(k)).dot((_p.block<3, 1>(3 * j, i)
										- _p.block<3, 1>(3 * k, i)))) / dist(j, l);
								A += weights_(j, k) * (Sl * dist(j, k) - Sk * dist(j, l))
										/ (dist(j, k) * dist(j, k));
							}
						}
						_w = -weights_(j, l) * A * w * w;
					}
					else
					{
						_w = 0;
						w = 0;
					}

					J.block<3, 1>(3 * j, i) -= p.col(l) * _w + _p.block<3, 1>(3 * l, i) * w;
				}
			}
		}
	}
#ifdef DEBUG_MODE
	std::cout << "IMesh Jacobians:" << J.rows() << "x" << J.cols() << "\n" << J << std::endl;
#endif
	return J.block(0, 0, 3 * N, N);
}
exotica::EReturn exotica::IMesh::setWeight(int i, int j, double weight)
{
	uint M = weights_.cols();
	if (i < 0 || i >= M || j < 0 || j >= M)
	{
		std::cout << "Invalid weight element (" << i << "," << j << "). Weight matrix " << M << "x"
				<< M << std::endl;
		return FAILURE;
	}
	if (weight < 0)
	{
		std::cout << "Invalid weight: " << weight << std::endl;
		return FAILURE;
	}
	weights_(i, j) = weight;
	return SUCCESS;
}

exotica::EReturn exotica::IMesh::setWeights(const Eigen::MatrixXd & weights)
{
	uint M = weights_.cols();
	if (weights.rows() != M || weights.cols() != M)
	{
		std::cout << "Invalid weight matrix (" << weights.rows() << "X" << weights.cols()
				<< "). Has to be" << M << "x" << M << std::endl;
		return FAILURE;
	}
	weights_ = weights;
	return SUCCESS;
}
