/*
 * CoM.cpp
 *
 *  Created on: 21 Mar 2014
 *      Author: yimingyang
 */

#include "kinematic_maps/CoM.h"

REGISTER_TASKMAP_TYPE("CoM", exotica::CoM);

exotica::CoM::CoM()
{
	initialised_ = false;

}

exotica::CoM::~CoM()
{
	//TODO
}

exotica::EReturn exotica::CoM::update(const Eigen::VectorXd & x)
{
	LOCK(lock_);

	invalidate();
	if (!initialised_)
	{
		return MMB_NIN;
	}

	//!< Temporaries
	bool success = true;
	Eigen::Vector3d phi;
	Eigen::MatrixXd jac;

	if (!robot_solver_.updateConfiguration(x))
	{
		return FAILURE;
	}

	if (!computeForwardMap(phi))
	{
		return FAILURE;
	}
	if (!computeJacobian(jac))
	{
		return FAILURE;
	}

	if (setPhi(phi) == setJacobian(jac) == SUCCESS)
	{
		return SUCCESS;
	}
	else
	{
		return FAILURE;
	}
	return SUCCESS;
}

bool exotica::CoM::computeForwardMap(Eigen::Vector3d & phi)
{
	if (!initialised_)
	{
		return false;
	}

	uint N = mass_.rows(), i;
	KDL::Frame tmp;
	KDL::Vector com = KDL::Vector::Zero();
	double M = mass_.sum();

	for (i = 0; i < N; i++)
	{
		tmp = base_pose_[i] * KDL::Frame(cog_[i]);
		com = com + mass_[i] * tmp.p;
	}
	phi.x() = com.x() / M;
	phi.y() = com.y() / M;
	phi.z() = com.z() / M;
	return true;
}

bool exotica::CoM::computeJacobian(Eigen::MatrixXd & jac)
{
	if (!initialised_)
	{
		return false;
	}
	if (!robot_solver_.generateForwardMap())
	{
		return false;
	}
	Eigen::MatrixXd eff_jac;
	if (!robot_solver_.generateJacobian(eff_jac))
	{
		return false;
	}
	uint N = eff_jac.rows() / 3, i, M = eff_jac.cols();
	if (mass_.size() != N)
	{
		return false;
	}
	jac = Eigen::MatrixXd::Zero(3, eff_jac.cols());
	for (i = 0; i < N; i++)
	{
		jac = jac + mass_[i] * eff_jac.block(3 * i, 0, 3, M);
	}
	return true;
}

exotica::EReturn exotica::CoM::initDerived(tinyxml2::XMLHandle & handle)
{
	std::string urdf_file;
	kinematica::SolutionForm_t solution;

	//!< Checks for compulsaries
	if (!handle.FirstChildElement("Urdf").ToElement())
	{
		INDICATE_FAILURE;
		return PAR_ERR;
	} //!< We must have a urdf specification at the very least
	if (!handle.FirstChildElement("Update").ToElement())
	{
		INDICATE_FAILURE;
		return PAR_ERR;
	} //!< We must have the list of joints

	//!< First the file
	urdf_file = handle.FirstChildElement("Urdf").ToElement()->GetText();
	if (urdf_file.empty())
	{
		return PAR_ERR;
	}

	//!< Now the solution params:
	solution.root_segment = "";
	solution.root_seg_off = KDL::Frame::Identity();
	if (handle.FirstChildElement("Root").ToElement())
	{
		if (!handle.FirstChildElement("Root").ToElement()->Attribute("segment"))
		{
			INDICATE_FAILURE;
			return PAR_ERR;
		}
		solution.root_segment = handle.FirstChildElement("Root").ToElement()->Attribute("segment");

		if (handle.FirstChildElement("Root").FirstChildElement("vector").ToElement())
		{
			Eigen::VectorXd temp_vector;
			if (exotica::getVector(*(handle.FirstChildElement("Root").FirstChildElement("vector").ToElement()), temp_vector)
					!= SUCCESS)
			{
				INDICATE_FAILURE;
				return PAR_ERR;
			}
			if (temp_vector.size() != 3)
			{
				INDICATE_FAILURE;
				return PAR_ERR;
			}
			solution.root_seg_off.p.x(temp_vector(0));
			solution.root_seg_off.p.y(temp_vector(1));
			solution.root_seg_off.p.z(temp_vector(2));
		}

		if (handle.FirstChildElement("Root").FirstChildElement("quaternion").ToElement())
		{
			Eigen::VectorXd temp_vector;
			if (exotica::getVector(*(handle.FirstChildElement("Root").FirstChildElement("quaternion").ToElement()), temp_vector)
					!= SUCCESS)
			{
				INDICATE_FAILURE;
				return PAR_ERR;
			}
			if (temp_vector.size() != 4)
			{
				INDICATE_FAILURE;
				return PAR_ERR;
			}
			solution.root_seg_off.M =
					KDL::Rotation::Quaternion(temp_vector(1), temp_vector(2), temp_vector(3), temp_vector(0));
		}
	}

	solution.zero_other_joints = true;  //!< By default it is true
	if (handle.FirstChildElement("Update").ToElement()->Attribute("zero_unnamed")) //!< If it exists
	{
		if (handle.FirstChildElement("Update").ToElement()->QueryBoolAttribute("zero_unnamed", &solution.zero_other_joints)
				!= tinyxml2::XML_NO_ERROR)
		{
			INDICATE_FAILURE;
			return PAR_ERR;
		}  //!< If exists but wrongly defined
	}
	tinyxml2::XMLHandle joint_handle(handle.FirstChildElement("Update").FirstChildElement("joint"));
	while (joint_handle.ToElement())
	{
		if (!joint_handle.ToElement()->Attribute("name"))
		{
			INDICATE_FAILURE;
			return PAR_ERR;
		} //!< If no name exists
		solution.joints_update.push_back(joint_handle.ToElement()->Attribute("name"));
		joint_handle = joint_handle.NextSiblingElement("joint");
	}
	if (solution.joints_update.size() < 1)
	{
		INDICATE_FAILURE;
		return PAR_ERR;
	}  //!< If no joints specified

	solution.ignore_unused_segs = true;
	if (handle.FirstChildElement("EndEffector").ToElement()->Attribute("ignore_unused"))
	{
		if (handle.FirstChildElement("EndEffector").ToElement()->QueryBoolAttribute("ignore_unused", &solution.ignore_unused_segs)
				!= tinyxml2::XML_NO_ERROR)
		{
			INDICATE_FAILURE;
			return PAR_ERR;
		}
	}
	tinyxml2::XMLHandle segment_handle(handle.FirstChildElement("EndEffector").FirstChildElement("limb"));
	while (segment_handle.ToElement())
	{
		if (!segment_handle.ToElement()->Attribute("segment"))
		{
			INDICATE_FAILURE;
			return PAR_ERR;
		}
		solution.end_effector_segs.push_back(segment_handle.ToElement()->Attribute("segment"));
		KDL::Frame temp_frame = KDL::Frame::Identity(); //!< Initialise to identity
		if (segment_handle.FirstChildElement("vector").ToElement())
		{
			Eigen::VectorXd temp_vector;
			if (exotica::getVector(*(segment_handle.FirstChildElement("vector").ToElement()), temp_vector)
					!= SUCCESS)
			{
				INDICATE_FAILURE;
				return PAR_ERR;
			}
			if (temp_vector.size() != 3)
			{
				INDICATE_FAILURE;
				return PAR_ERR;
			}
			temp_frame.p.x(temp_vector(0));
			temp_frame.p.y(temp_vector(1));
			temp_frame.p.z(temp_vector(2));
		}
		if (segment_handle.FirstChildElement("quaternion").ToElement())
		{
			Eigen::VectorXd temp_vector;
			if (exotica::getVector(*(segment_handle.FirstChildElement("quaternion").ToElement()), temp_vector)
					!= SUCCESS)
			{
				INDICATE_FAILURE;
				return PAR_ERR;
			}
			if (temp_vector.size() != 4)
			{
				INDICATE_FAILURE;
				return PAR_ERR;
			}
			temp_frame.M =
					KDL::Rotation::Quaternion(temp_vector(1), temp_vector(2), temp_vector(3), temp_vector(0));
		}
		solution.end_effector_offs.push_back(temp_frame);
		segment_handle = segment_handle.NextSiblingElement("limb");
	}

	LOCK(lock_);
	if (!robot_solver_.initKinematics(urdf_file, solution))
	{
		return FAILURE;
	}

	if(!changeEffToCoM(solution.end_effector_segs))
	{
		return FAILURE;
	}
	return SUCCESS;
}

bool exotica::CoM::changeEffToCoM(const std::vector<std::string> & names)
{
	if (!robot_solver_.getCoMProperties(mass_, cog_, tip_pose_, base_pose_))
	{
		return false;
	}
	std::vector<KDL::Frame> com_offs;
	uint N = names.size(), i;
	for (i = 0; i < N; i++)
	{
		com_offs[i] = tip_pose_[i].Inverse() * base_pose_[i] * KDL::Frame(cog_[i]);
	}
	kinematica::SolutionForm_t new_solution;
	new_solution.end_effector_segs=names;
	new_solution.end_effector_offs=com_offs;
	if(!robot_solver_.updateEndEffectors(new_solution))
	{
		return false;
	}
	return true;
}
