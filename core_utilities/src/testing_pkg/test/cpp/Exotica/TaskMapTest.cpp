#include "testing_pkg/Exotica/TaskMapTest.h"


TEST_F(TaskMapTest, XIT)  //!< XML Initialisation Test
{
  tinyxml2::XMLDocument doc;
  tinyxml2::XMLHandle xml_handle(doc);
  
  ASSERT_EQ(tinyxml2::XML_NO_ERROR, doc.LoadFile((resource_path_ + std::string("DTaskMap.xml")).c_str()));
  xml_handle = tinyxml2::XMLHandle(doc.RootElement());
  EXPECT_EQ(exotica::SUCCESS, (exotica::TaskMapCreator::Instance().createObject("DTaskMap", base_ptr_)));
  EXPECT_EQ(0, base_ptr_->type().compare("testing::DTaskMap"));
  EXPECT_EQ(exotica::SUCCESS, (base_ptr_->initBase(xml_handle)));
  EXPECT_TRUE(boost::dynamic_pointer_cast<testing::DTaskMap>(base_ptr_)->derived_called);
}


TEST_F(TaskMapTest, DMT)  //!< Derived-task Mapping Tests: will contain tests to check whether the jacobian is correctly computed through finite-differences method
{
  for (int i=0; i<registered_types_.size(); i++)  //!< Iterate through the registered tasks
  {
    std::string             xml_path;
    tinyxml2::XMLDocument   xml_document;
    tinyxml2::XMLHandle     xml_handle(xml_document);
    
    if (exotica::XMLTester::Instance().getTest(registered_types_[i], xml_path) == exotica::SUCCESS)  //!< If it is wished to be tested...
    {
      //!< Prepare
      
      if (xml_document.LoadFile((resource_path_ + xml_path).c_str()) != tinyxml2::XML_NO_ERROR) //!< Attempt to load file
      {
        ADD_FAILURE() << " : Initialiser for " << registered_types_[i] << " (file: "<<resource_path_ + xml_path <<") is invalid."; //!< Have to use this method to add failure since I do not want to continue for this object but do not wish to abort for all the others...
        continue; //!< Go to next object
      }
      
      if (exotica::TaskMapCreator::Instance().createObject(registered_types_[i], base_ptr_))  //!< Could not create
      {
        ADD_FAILURE() << " : Could not create object of type " << registered_types_[i];
        continue;
      }
      
      xml_handle = tinyxml2::XMLHandle(xml_document.RootElement());
      
      if (base_ptr_->initBase(xml_handle))  //!< Something wrong in initialisation
      {
        
        ADD_FAILURE() << " : XML Initialiser is malformed for " << registered_types_[i] << "\nFile name: " << xml_path;
        continue;
      }
      
      //!< Test
      xml_handle = tinyxml2::XMLHandle(xml_handle.FirstChildElement("TestPoint"));  //!< Locate the first test-point
      
      if (!xml_handle.ToElement())
      {
        ADD_FAILURE() << " : XML Tester is malformed for " << registered_types_[i];
        continue;
      }
      
      int j=0;
      
      while (xml_handle.ToElement())
      {
        
        Eigen::VectorXd conf_space; //!< Vector containing the configuration point for testing
        Eigen::VectorXd task_space; //!< Vector with the task-space co-ordinate of the forward map
        Eigen::VectorXd phi;        //!< The computed phi
        Eigen::MatrixXd jacobian;   //!< The Jacobian computation
        
        //!< Prepare
        if (!xml_handle.FirstChildElement("ConfSpace").ToElement()) //!< If inexistent
        {
          ADD_FAILURE() << " : Missing Config for " << registered_types_[i] << " @ iteration " << j;
          xml_handle = tinyxml2::XMLHandle(xml_handle.NextSiblingElement("TestPoint"));
          j++;
          continue; //!< With the inner while loop...
        }
        
        if (exotica::getVector(*(xml_handle.FirstChildElement("ConfSpace").ToElement()), conf_space) != exotica::SUCCESS)
        {
          ADD_FAILURE() << " : Could not parse Config Vector for " << registered_types_[i] << " @ iteration " << j;
          xml_handle = tinyxml2::XMLHandle(xml_handle.NextSiblingElement("TestPoint"));
          j++;
          continue; //!< With the inner while loop...
        }
        
        if (!xml_handle.FirstChildElement("TaskSpace").ToElement()) //!< If inexistent
        {
          ADD_FAILURE() << " : Missing Task Space point for " << registered_types_[i] << " @ iteration " << j;
          xml_handle = tinyxml2::XMLHandle(xml_handle.NextSiblingElement("TestPoint"));
          j++;
          continue; //!< With the inner while loop...
        }
        
        if (exotica::getVector(*(xml_handle.FirstChildElement("TaskSpace").ToElement()), task_space) != exotica::SUCCESS)
        {
          ADD_FAILURE() << " : Could not parse task vector for " << registered_types_[i] << " @ iteration " << j;
          xml_handle = tinyxml2::XMLHandle(xml_handle.NextSiblingElement("TestPoint"));
          j++;
          continue; //!< With the inner while loop...
        }
        
        if (base_ptr_->update(conf_space) != exotica::SUCCESS)
        {
          ADD_FAILURE() << " for " << registered_types_[i] << " @ iteration " << j;
          xml_handle = tinyxml2::XMLHandle(xml_handle.NextSiblingElement("TestPoint"));
          j++;
          continue; //!< With the inner while loop...
        }
        
        if (base_ptr_->phi(phi) != exotica::SUCCESS)
        {
          ADD_FAILURE() << " for " << registered_types_[i] << " @ iteration " << j;
          xml_handle = tinyxml2::XMLHandle(xml_handle.NextSiblingElement("TestPoint"));
          j++;
          continue; //!< With the inner while loop...
        }
        
        if (base_ptr_->jacobian(jacobian) != exotica::SUCCESS)
        {
          ADD_FAILURE() << " for " << registered_types_[i] << " @ iteration " << j;
          xml_handle = tinyxml2::XMLHandle(xml_handle.NextSiblingElement("TestPoint"));
          j++;
          continue; //!< With the inner while loop...
        }
        
        //!< Check the Forward mapping
        EXPECT_TRUE(compareVectors(phi, task_space, TOLERANCE)) << " : Phi mismatch for " << registered_types_[i] << " @ iteration " << j;
        
        //!< Now the Jacobian (finite differences method)
        for (int k=0; k<conf_space.size(); k++)
        {
          
          Eigen::VectorXd temp_phi;
          Eigen::VectorXd conf_perturb = conf_space;
          conf_perturb(k) += EPSILON;
          
          if (base_ptr_->update(conf_perturb) != exotica::SUCCESS) { ADD_FAILURE() << " for " << registered_types_[i] << " @ iteration " << j << " in config-dimension " << k; continue; }
          
          if (base_ptr_->phi(temp_phi) != exotica::SUCCESS) { ADD_FAILURE() << " for " << registered_types_[i] << " @ iteration " << j << " in config-dimension " << k; continue; }
          
          if (temp_phi.size() != phi.size()) { ADD_FAILURE() << " for " << registered_types_[i] << " @ iteration " << j << " in config-dimension " << k; continue; }
          
          Eigen::VectorXd task_diff = (temp_phi - phi)/EPSILON;  //!< Final - initial
          
          Eigen::VectorXd jac_column = jacobian.col(k); //!< Get the desired column (for the current joint)
          
          EXPECT_TRUE(compareVectors(task_diff, jac_column, TOLERANCE)) << " Incorrect for " << registered_types_[i] << " @ iteration " << j << " in config-dimension " << k << task_diff << "\n" << jac_column;
        }
        
        xml_handle = tinyxml2::XMLHandle(xml_handle.NextSiblingElement("TestPoint"));
        j++;
        
      }
      
    }
    
  }
  
}
