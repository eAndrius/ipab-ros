#include "testing_pkg/Exotica/TaskDefinitionTest.h"

using namespace testing;

TEST_F(TaskDefinitionTest, DIT)  //!< Default Initialisation Test
{
  Eigen::VectorXd y, y2;
  Eigen::MatrixXd y_dot, y_dot2;
  
  //!< Attempt access without setting pointer
  EXPECT_EQ(exotica::MEM_ERR, problem_comp_.phi(y));
  EXPECT_EQ(exotica::MEM_ERR, problem_comp_.jacobian(y_dot));
  
  //!< Attempt access with valid pointer but no mapping computed
  EXPECT_EQ(exotica::SUCCESS, problem_comp_.setTaskMap(task_ptr_));
  EXPECT_EQ(exotica::MMB_NIN, problem_comp_.phi(y));
  EXPECT_EQ(exotica::MMB_NIN, problem_comp_.jacobian(y_dot));
  
  //!< Attempt proper use
  EXPECT_EQ(exotica::SUCCESS, task_ptr_->update(Eigen::VectorXd::Zero(3)));
  ASSERT_EQ(exotica::SUCCESS, problem_comp_.phi(y));
  ASSERT_EQ(exotica::SUCCESS, problem_comp_.jacobian(y_dot));
  ASSERT_EQ(exotica::SUCCESS, task_ptr_->phi(y2));
  ASSERT_EQ(exotica::SUCCESS, task_ptr_->jacobian(y_dot2));
  EXPECT_TRUE(compareVectors(y, y2));
  EXPECT_TRUE(compareMatrices(y_dot, y_dot2));
}
