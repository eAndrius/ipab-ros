#ifndef TESTING_TASK_MAP_TEST_H
#define TESTING_TASK_MAP_TEST_H

#include <exotica/EXOTica.hpp>
#include <tinyxml2/tinyxml2.h>
#include <gtest/gtest.h>
#include <Eigen/Eigen>
#include <cmath>

#include "testing_pkg/TestingTools.h"

/** Type definitions first **/
namespace testing
{
  class DTaskMap : public exotica::TaskMap //!< A concrete implementation of the TaskMap class
  {
    public:
      DTaskMap ()  //!< Default constructor
      {
        derived_called = false;
        update_called = false;
      };
      
      void clearFlags() { derived_called = update_called = false; }
      
      virtual exotica::EReturn update(const Eigen::VectorXd & x)
      {
        update_called = true; //!< Indicate entry into function
        if (x.size() != 3) { return exotica::PAR_ERR; }
        Eigen::VectorXd phi(1);
        phi(0) = std::pow(x[0],3) + 2*std::pow(x[1],2) - std::pow(x[2],4);
        exotica::EReturn temp_return = setPhi(phi);
        if (temp_return) { return temp_return; }
        Eigen::MatrixXd jacobian(1,3);
        jacobian(0,0) = 3*std::pow(x[0],2);
        jacobian(0,1) = 4*x[1];
        jacobian(0,2) = -4*std::pow(x[2],3);
        temp_return = setJacobian(jacobian);
        return temp_return;
      }
    
      bool derived_called;
      bool update_called;
      
    protected:
      virtual exotica::EReturn initDerived(tinyxml2::XMLHandle & handle)
      {
        derived_called = true;
        return exotica::SUCCESS;
      }
  
  };
}

REGISTER_TASKMAP_TYPE("DTaskMap", testing::DTaskMap); //!< Register the class
REGISTER_FOR_XML_TEST("DTaskMap", "DTaskMap.xml");    //!< Register for testing

class TaskMapTest : public ::testing::Test  //!< Testing class for the exotica library
{
  public: 
  
    virtual void SetUp()
    {
      ASSERT_EQ(exotica::SUCCESS, (exotica::TaskMapCreator::Instance().listImplementations(registered_types_)));  //!< Ensure that the object loads
      std::string package_path;
      ASSERT_TRUE(findPackagePath("testing_pkg", package_path));  //!< Removes dependency on ros (in the future)
      resource_path_ = package_path.append("/resource/Exotica/");
    };
    
    virtual void TearDown() {};
  
    //!< Member Variables
    std::vector<std::string>                registered_types_;
    std::string                             resource_path_;
    boost::shared_ptr<exotica::TaskMap>    base_ptr_;
    
};

#endif
