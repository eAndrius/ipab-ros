#ifndef TESTING_PROBLEM_COMPONENT_TEST_H
#define TESTING_PROBLEM_COMPONENT_TEST_H

#include <exotica/EXOTica.hpp>
#include <tinyxml2/tinyxml2.h>
#include <gtest/gtest.h>
#include <Eigen/Eigen>

#include "testing_pkg/TestingTools.h"

namespace testing
{
  class DTaskDefinition : public exotica::TaskDefinition
  {
    protected:
      exotica::EReturn initDerived(tinyxml2::XMLHandle & handle)
      {
        return exotica::SUCCESS;
      }
  };
}
class TaskDefinitionTest : public ::testing::Test
{
  public:
    virtual void SetUp()
    {
      ASSERT_EQ(exotica::SUCCESS, (exotica::TaskMapCreator::Instance().createObject("DTaskMap", task_ptr_))) << "No Default TaskMap class";  //!< Ensure that there is a default TaskMap for testing
      std::string package_path;
      ASSERT_TRUE(findPackagePath("testing_pkg", package_path));  //!< Removes dependency on ros (in the future)
      resource_path_ = package_path.append("/resource/Exotica/");
    };
    
    virtual void TearDown() {};
  
    //!< Member Variables
    std::string                          resource_path_;
    boost::shared_ptr<exotica::TaskMap>  task_ptr_;
    testing::DTaskDefinition             problem_comp_;
};

#endif
