/*
 * kinematic_scene.h
 *
 *  Created on: 12 Mar 2014
 *      Author: yiming
 */

#ifndef KINEMATIC_SCENE_H_
#define KINEMATIC_SCENE_H_

#include <ros/ros.h>
#include <boost/thread/mutex.hpp>
#include <Eigen/Eigen>
#include <string>
#include <kinematica/KinematicTree.h>
#include <geometry_msgs/PoseStamped.h>
#include <moveit_msgs/PlanningScene.h>
#include <moveit/planning_scene/planning_scene.h>
#include <moveit/robot_state/robot_state.h>
#include <moveit/robot_model/robot_model.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit_msgs/GetPlanningScene.h>
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/GetStateValidity.h>
#include <moveit_msgs/DisplayRobotState.h>
#include <geometry_msgs/Pose.h>
#include <eigen_conversions/eigen_kdl.h>
#include <map>
namespace kinematica {

/** Kinematic Scene Object */
struct KinematicSceneForm_t {
	kinematica::SolutionForm_t optimisation;
	std::map<std::string, std::string> eff_world_map;
};
class KinematicScene {
public:
	/**
	 * @brief	Constructor of Kinematic Scene
	 * @param	world_frame		Name of reference frame
	 */
	//KinematicScene(const std::string name, const KDL::Frame & world_frame, const std::string robot_description);
	KinematicScene(const std::string name, const std::string group_name,
			const std::string scene_topic);

	/**
	 * @brief	Initialisation of KS
	 * @param	urdf_file	Path to robot urdf file
	 * @param	optimisation	Optimisation parameters
	 * @param	world_obj	World objects
	 */
	bool initKinematicScene(const std::string & urdf_file,
			const KinematicSceneForm_t & ks_form);

	/**
	 * @brief	Update the configuration of the robot
	 * @return	True if succeeded, false otherwise
	 */
	bool update(const Eigen::VectorXd & joint_configuration);

	/**
	 * @brief	Get Forward Map
	 * @param	phi		Forward Map
	 * @return	True if succeeded, false otherwise
	 */
	bool getForwardMap(Eigen::VectorXd & phi);

	/**
	 * @brief	Get Jacobian
	 * @param	jac		Jacobian matrix
	 * @return	True if succeeded, false otherwise
	 */
	bool getJacobian(Eigen::MatrixXd & jac);

	/**
	 * @brief	Get Poses of world objects
	 * @param	names	Names of the objects (can be robot links, or world objects)
	 * @param	poses	Poses of the objects
	 * @return	True if all poses are available, false otherwise
	 */
	bool getPoses(const std::vector<std::string> names,
			std::vector<KDL::Frame> poses);

	/**
	 * @brief	We may need to publish things to the scene,
	 * 			if we dont have a real robot or simulation running and want to see the motion
	 */
	void startPublishing();
	void stopPublishing();
	void publishScene();
	//private:
	/**
	 * @brief	Since we are using moveit scene, we need to update to the newest state before each planning
	 */
	bool updateScene();
	void sceneCallback(const moveit_msgs::PlanningScene::ConstPtr & scene);

	/**
	 * @brief	Update Scene to Kinematica
	 */
	bool updateSceneToKinematica();

	void
	void KS_INFO(std::string info);
	ros::NodeHandle nh_;
	ros::AsyncSpinner spinner_;
	ros::Subscriber scene_sub_;
	ros::Publisher scene_pub_;
	ros::ServiceClient get_scene_client_;
	moveit_msgs::GetPlanningScene get_scene_srv_;
	bool initialised_; /** Initialisation flag */
	kinematica::KinematicTree kinematic_solver_; /** Kinematica Solver */
	//robot_state::RobotStatePtr robot_state_;
	//const robot_model::JointModelGroup* group_;
	planning_scene::PlanningScenePtr ps_;
	std::map<std::string, std::string> eff_world_map_;
	boost::mutex locker_;
	bool isPublishing_;
};
}
//namespace

#endif /* KINEMATIC_SCENE_H_ */
