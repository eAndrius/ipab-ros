/*
 * kinematic_scene.h
 *
 *  Created on: 12 Mar 2014
 *      Author: yiming
 */

#ifndef KINEMATIC_SCENE_H_
#define KINEMATIC_SCENE_H_

#include <ros/ros.h>
#include <boost/thread/mutex.hpp>
#include <Eigen/Eigen>
#include <string>
#include <kinematica/KinematicTree.h>
#include <geometry_msgs/PoseStamped.h>
#include <moveit_msgs/PlanningScene.h>
#include <moveit/planning_scene/planning_scene.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit_msgs/GetPlanningScene.h>
#include <geometry_msgs/Pose.h>
#include <eigen_conversions/eigen_kdl.h>
#include <map>

#define INF 9999
namespace kinematica
{

	/** Kinematic Scene Initialisation Form */
	struct KinematicSceneForm_t
	{
			kinematica::SolutionForm_t optimisation;			//!< Kinematica solution form
			std::map<std::string, std::string> eff_world_map;	//!<end-effector to world object mapping
			std::vector<std::string> ext_ids;					//!< External object names that might be used (e.g. iMesh)
	};
	enum KS_EDIT_TYPE
	{
		ADD_WORLD = 0, ADD_ROBOT = 1, REMOVE_WORLD = 2, REMOVE_ROBOT = 2
	};
	struct KS_Object
	{
			std::string name;	//!< Object name
			std::string ref;	//!< Reference frame
			geometry_msgs::Pose origin;	//!< Origin of the object
			shape_msgs::SolidPrimitive shape;	//!< Object type
			KS_EDIT_TYPE type;	//!< Edit action type

	};

	class KinematicScene
	{
		public:
			/**
			 * @brief	Constructor of Kinematic Scene
			 * @param	world_frame		Name of reference frame
			 */
			//KinematicScene(const std::string name, const KDL::Frame & world_frame, const std::string robot_description);
			KinematicScene(const std::string name, const std::string group_name,
					const std::string scene_topic);

			/**
			 * @brief	Initialisation of KS
			 * @param	urdf_file	Path to robot urdf file
			 * @param	optimisation	Optimisation parameters
			 * @param	world_obj	World objects
			 */
			bool initKinematicScene(const std::string & urdf_file,
					const KinematicSceneForm_t & ks_form);

			/**
			 * @brief	Update the configuration of the robot
			 * @return	True if succeeded, false otherwise
			 */
			bool update(const Eigen::VectorXd & joint_configuration);

			/**
			 * @brief	Get Forward Map (Wrap both joints and external objects, the object forward position
			 * 			will not be included in the result if the transform is unknown, and its name will be
			 * 			listed in the unknown objects)
			 * @param	phi				Forward Map
			 * @param	unkonwn_objects	Objects that unknown to the scene
			 * @return	True if succeeded, false otherwise
			 */
			bool getForwardMap(Eigen::VectorXd & phi,std::vector<std::string> & unknown_objects);

			/**
			 * @brief	Get Jacobian
			 * @param	jac		Jacobian matrix
			 * @return	True if succeeded, false otherwise
			 */
			bool getJacobian(Eigen::MatrixXd & jac);

			/**
			 * @brief	Get Poses of world objects
			 * @param	names	Names of the objects (can be robot links, or world objects)
			 * @param	poses	Poses of the objects
			 * @return	True if all poses are available, false otherwise
			 */
			bool getPoses(const std::vector<std::string> names, std::vector<KDL::Frame> poses);

			/**
			 * @brief	Add new objects to kinematic scene
			 * @param	object	KS_objects
			 * @return	True if succeeded, false otherwise
			 */
			bool addObject(const KS_Object object);
			/**
			 * @brief	We may need to publish things to the scene,
			 * 			if we dont have a real robot or simulation running and want to see the motion
			 */
			void startPublishing();
			void stopPublishing();
			void publishScene();

			/**
			 * @brief	Since we are using moveit scene, we need to update to the newest state before each planning
			 */
			bool updateScene();
		private:

			void sceneCallback(const moveit_msgs::PlanningScene::ConstPtr & scene);

			/**
			 * @brief	Update Scene to Kinematica (update attached object offsets)
			 */
			bool updateSceneToKinematica();

			void KS_INFO(std::string info);
			ros::NodeHandle nh_;	//!< Internal ros node handle
			ros::AsyncSpinner spinner_;	//!< Spinner
			ros::Publisher scene_pub_;	//!< Planning scene publisher
			ros::ServiceClient get_scene_client_;//!< Action client for the asking moveit planning scene
			moveit_msgs::GetPlanningScene get_scene_srv_;//!< Action server for the asking moveit planning scene
			bool initialised_; /** Initialisation flag */
			kinematica::KinematicTree kinematic_solver_; /** Kinematica Solver */
			planning_scene::PlanningScenePtr ps_;	//!< Internal planning scene
			std::map<std::string, std::string> eff_world_map_;//!< Internally store the end-effectors to world objects map
			std::string task_root_;	//!< The task root
			std::vector<std::string> ext_ids_;	//!< Names of external objects
			boost::mutex locker_;	//!< Mutex
			bool isPublishing_;	//!< Publishing scene flag
	};
}

#endif /* KINEMATIC_SCENE_H_ */
