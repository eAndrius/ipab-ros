/*
 * kinematic_scene.cpp
 *
 *  Created on: 13 Mar 2014
 *      Author: yiming
 */
#include "k_scene/kinematic_scene.h"

#define DEBUG_MODE
kinematica::KinematicScene::KinematicScene(const std::string name,
		const std::string group_name, const std::string scene_topic) :
		nh_("K_Scene"), spinner_(1), ps_(
				new planning_scene::PlanningScene(
						robot_model_loader::RobotModelLoader(
								"robot_description").getModel())) {
	initialised_ = false;
	scene_sub_ = nh_.subscribe<moveit_msgs::PlanningScene>(scene_topic, 1,
			boost::bind(&kinematica::KinematicScene::sceneCallback, this, _1));
	scene_pub_ = nh_.advertise<moveit_msgs::PlanningScene>("planning_scene", 1,
			true);
	spinner_.start();
	get_scene_client_ = nh_.serviceClient<moveit_msgs::GetPlanningScene>(
			"/get_planning_scene");

	get_scene_srv_.request.components.components =
			get_scene_srv_.request.components.SCENE_SETTINGS
					| get_scene_srv_.request.components.ROBOT_STATE
					| get_scene_srv_.request.components.ROBOT_STATE_ATTACHED_OBJECTS
					| get_scene_srv_.request.components.WORLD_OBJECT_NAMES
					| get_scene_srv_.request.components.WORLD_OBJECT_GEOMETRY
					| get_scene_srv_.request.components.OCTOMAP
					| get_scene_srv_.request.components.TRANSFORMS
					| get_scene_srv_.request.components.ALLOWED_COLLISION_MATRIX
					| get_scene_srv_.request.components.LINK_PADDING_AND_SCALING
					| get_scene_srv_.request.components.OBJECT_COLORS;

	updateScene();

	//robot_state_.reset(new robot_state::RobotState(robot_model_));
	//group_ = robot_state_->getJointModelGroup(group_name);
	KS_INFO("New Kinematic Scene (" + ps_->getName() + ") Constructed");
}

bool kinematica::KinematicScene::initKinematicScene(
		const std::string & urdf_file,
		const kinematica::KinematicSceneForm_t & ks_form) {
	/** We need to change the root offset with respect to the world frame */
	kinematica::SolutionForm_t tmp_opt = ks_form.optimisation;
	if (!ps_->knowsFrameTransform("/" + ks_form.optimisation.root_segment)) {
#ifdef DEBUG_MODE
		KS_INFO(
				"World frame: (" + ps_->getPlanningFrame() + ") to root frame ("
						+ ks_form.optimisation.root_segment
						+ ") is not available.");
		return false;
#endif
	}

	tf::transformEigenToKDL(
			ps_->getFrameTransform("/" + ks_form.optimisation.root_segment),
			tmp_opt.root_seg_off);

	if (kinematic_solver_.initKinematics(urdf_file, tmp_opt)) {
#ifdef DEBUG_MODE
		KS_INFO("Kinematica Initialised");
#endif
		initialised_ = true;
	} else {
		KS_INFO("Kinematica Initialisation Failed");
		initialised_ = false;
		return false;
	}
	eff_world_map_ = ks_form.eff_world_map;
	return true;
}

bool kinematica::KinematicScene::update(
		const Eigen::VectorXd & joint_configuration) {
	if (!kinematic_solver_.updateConfiguration(joint_configuration)) {
#ifdef DEBUG_MODE
		KS_INFO("Kinematica Update Failed");
#endif
		return false;
	}

	if (!updateSceneToKinematica()) {
#ifdef DEBUG_MODE
		KS_INFO("Update Scene to Kinematica Failed");
		return false;
#endif
	}

	if (!kinematic_solver_.generateForwardMap()) {
#ifdef DEBUG_MODE
		KS_INFO("Generating Forward Mapping Failed");
#endif
		return false;
	}
	if (!kinematic_solver_.generateJacobian()) {
#ifdef DEBUG_MODE
		KS_INFO("Generating Forward Mapping Failed");
#endif
		return false;
	}
	//robot_state_.setJointGroupPositions(group_, joint_configuration);
	return true;
}

bool kinematica::KinematicScene::getForwardMap(Eigen::VectorXd & phi) {
//	if (!kinematic_solver_.getPhi(phi))
//	{
//#ifdef DEBUG_MODE
//		KS_INFO("Getting Forward Mapping Failed");
//#endif
//		return false;
//	}
	return true;
}

bool kinematica::KinematicScene::getJacobian(Eigen::MatrixXd & jac) {
//	if (!kinematic_solver_.getJacobian(jac))
//	{
//#ifdef DEBUG_MODE
//		KS_INFO("Getting Forward Mapping Failed");
//#endif
//		return false;
//	}
	return true;
}

bool kinematica::KinematicScene::updateSceneToKinematica() {
	std::map<std::string, std::string>::iterator it;
	for (it = eff_world_map_.begin(); it != eff_world_map_.end(); ++it) {
		if (!ps_->knowsFrameTransform(it->second)
				|| !ps_->knowsFrameTransform(it->first)) {
#ifdef DEBUG_MODE
			KS_INFO("Frame Transform of " + it->second + " is unknown");
#endif
			return false;
		}
		KDL::Frame eef_w, link_w, eef_link;
		tf::transformEigenToKDL(ps_->getFrameTransform(it->first), link_w);
		tf::transformEigenToKDL(ps_->getFrameTransform(it->second), eef_w);
		eef_link = link_w.Inverse() * eef_w;

		/** Now we update Kinematica offset */
	}
	return true;
}

bool kinematica::KinematicScene::getPoses(const std::vector<std::string> names,
		std::vector<KDL::Frame> poses) {
	uint size = names.size(), i;
	poses.resize(size);
	for (i = 0; i < size; i++) {
		if (kinematic_solver_.getPose(names[i], poses[i])) {
			continue;
		} else if (ps_->knowsFrameTransform(names[i])) {

			tf::transformEigenToKDL(ps_->getFrameTransform(names[i]), poses[i]);
		} else {
#ifdef DEBUG_MODE
			KS_INFO("Frame Transform of (" + names[i] + ") is not available");
#endif
			poses.resize(0);
			return false;
		}
	}
	return true;
}
void kinematica::KinematicScene::startPublishing() {
#ifdef DEBUG_MODE
	KS_INFO("Start Publishing Robot State To The Scene");
#endif
	boost::mutex::scoped_lock(locker_);
	isPublishing_ = true;
}

void kinematica::KinematicScene::stopPublishing() {
#ifdef DEBUG_MODE
	KS_INFO("Sto Publishing Robot State To The Scene");
#endif
	boost::mutex::scoped_lock(locker_);
	isPublishing_ = true;
}

void kinematica::KinematicScene::publishScene() {
	moveit_msgs::AttachedCollisionObject attached_object;
	attached_object.link_name = "right_wrist";
	/* The header must contain a valid TF frame*/
	attached_object.object.header.frame_id = "right_wrist";
	/* The id of the object */
	attached_object.object.id = "box";

	/* A default pose */
	geometry_msgs::Pose pose;
	pose.orientation.w = 1.0;
	pose.position.x = 0.3;
	/* Define a box to be attached */
	shape_msgs::SolidPrimitive primitive;
	primitive.type = primitive.BOX;
	primitive.dimensions.resize(3);
	primitive.dimensions[0] = 0.1;
	primitive.dimensions[1] = 0.1;
	primitive.dimensions[2] = 0.1;

	attached_object.object.primitives.push_back(primitive);
	attached_object.object.primitive_poses.push_back(pose);

	// Note that attaching an object to the robot requires
	// the corresponding operation to be specified as an ADD operation
	attached_object.object.operation = attached_object.object.ADD;

	moveit_msgs::PlanningScene planning_scene;
	ps_->getPlanningSceneMsg(planning_scene);
	planning_scene.world.collision_objects.push_back(attached_object.object);
	scene_pub_.publish(planning_scene);
	KS_INFO("Publishing Scene");
}

bool kinematica::KinematicScene::updateScene() {
	boost::mutex::scoped_lock(locker_);
	if (!get_scene_client_.call(get_scene_srv_)) {
		KS_INFO("Cant get Planning Scene");
		return false;
	}
	ps_->usePlanningSceneMsg(get_scene_srv_.response.scene);
	return true;
}
void kinematica::KinematicScene::sceneCallback(
		const moveit_msgs::PlanningScene::ConstPtr & scene) {
#ifdef DEBUG_MODE
	KS_INFO("Setting Planning Scene From Moveit (" + scene->name + ").");
#endif
	boost::mutex::scoped_lock(locker_);
		ps_->setPlanningSceneDiffMsg(*scene);
}
void kinematica::KinematicScene::KS_INFO(std::string info) {
	std::cout << "Kinematic Scene: " << info << std::endl;
}
