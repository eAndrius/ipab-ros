/*
 * kinematic_scene.cpp
 *
 *  Created on: 13 Mar 2014
 *      Author: yiming
 */
#include "k_scene/kinematic_scene.h"
#define DEBUG_MODE
kinematica::KinematicScene::KinematicScene(const std::string name, const std::string group_name,
		const std::string scene_topic) :
				nh_("K_Scene"),
				spinner_(1),
				ps_(new planning_scene::PlanningScene(robot_model_loader::RobotModelLoader("robot_description").getModel()))
{
	initialised_ = true;
	scene_pub_ = nh_.advertise<moveit_msgs::PlanningScene>("planning_scene", 1, true);
	spinner_.start();
	get_scene_client_ = nh_.serviceClient<moveit_msgs::GetPlanningScene>("/get_planning_scene");
	get_scene_srv_.request.components.components = get_scene_srv_.request.components.SCENE_SETTINGS
			| get_scene_srv_.request.components.ROBOT_STATE
			| get_scene_srv_.request.components.ROBOT_STATE_ATTACHED_OBJECTS
			| get_scene_srv_.request.components.WORLD_OBJECT_NAMES
			| get_scene_srv_.request.components.WORLD_OBJECT_GEOMETRY
			| get_scene_srv_.request.components.OCTOMAP
			| get_scene_srv_.request.components.TRANSFORMS
			| get_scene_srv_.request.components.ALLOWED_COLLISION_MATRIX
			| get_scene_srv_.request.components.LINK_PADDING_AND_SCALING
			| get_scene_srv_.request.components.OBJECT_COLORS;

	updateScene();
	KS_INFO("New Kinematic Scene (" + ps_->getName() + ") Constructed");
}

bool kinematica::KinematicScene::initKinematicScene(const std::string & urdf_file,
		const kinematica::KinematicSceneForm_t & ks_form)
{
	/** We need to change the root offset with respect to the world frame */
	kinematica::SolutionForm_t tmp_opt = ks_form.optimisation;
	if (!ps_->knowsFrameTransform("/" + ks_form.optimisation.root_segment))
	{
#ifdef DEBUG_MODE
		KS_INFO("World frame: (" + ps_->getPlanningFrame() + ") to root frame ("
				+ ks_form.optimisation.root_segment + ") is not available.");
		return false;
#endif
	}

	tf::transformEigenToKDL(ps_->getFrameTransform("/" + ks_form.optimisation.root_segment), tmp_opt.root_seg_off);

	if (kinematic_solver_.initKinematics(urdf_file, tmp_opt))
	{
#ifdef DEBUG_MODE
		KS_INFO("Kinematica Initialised");
#endif
		initialised_ = true;
	}
	else
	{
		KS_INFO("Kinematica Initialisation Failed");
		initialised_ = false;
		return false;
	}
	task_root_ = ks_form.optimisation.root_segment;
	eff_world_map_ = ks_form.eff_world_map;
	ext_ids_ = ks_form.ext_ids;
	return true;
}

bool kinematica::KinematicScene::update(const Eigen::VectorXd & joint_configuration)
{
	updateScene();
	if (!kinematic_solver_.updateConfiguration(joint_configuration))
	{
#ifdef DEBUG_MODE
		KS_INFO("Kinematica Update Failed");
#endif
		return false;
	}
	if (!updateSceneToKinematica())
	{
#ifdef DEBUG_MODE
		KS_INFO("Update Scene to Kinematica Failed");
		return false;
#endif
	}
	if (!kinematic_solver_.generateForwardMap())
	{
#ifdef DEBUG_MODE
		KS_INFO("Generating Forward Mapping Failed");
#endif
		return false;
	}
	if (!kinematic_solver_.generateJacobian())
	{
#ifdef DEBUG_MODE
		KS_INFO("Generating Forward Mapping Failed");
#endif
		return false;
	}
	return true;
}

bool kinematica::KinematicScene::getForwardMap(Eigen::VectorXd & phi,
		std::vector<std::string> & unknown_objects)
{
	boost::mutex::scoped_lock(locker_);
	unknown_objects.clear();
	Eigen::VectorXd tmp_joint;
	if (!kinematic_solver_.getPhi(tmp_joint))
		return false;

	uint E = ext_ids_.size(), i;
	Eigen::VectorXd tmp_ext(3 * E);
	KDL::Frame root_w, ext_w, ext_root;
	if (!ps_->knowsFrameTransform("/" + task_root_))
	{
#ifdef DEBUG_MODE
		KS_INFO("Root transform [" + task_root_ + "] is unknown");
#endif
		return false;
	}
	tf::transformEigenToKDL(ps_->getFrameTransform("/" + task_root_), root_w);
	for (i = 0; i < E; i++)
	{
		if (!ps_->knowsFrameTransform("/" + ext_ids_[i]))
		{
#ifdef DEBUG_MODE
			KS_INFO("Object transform [" + ext_ids_[i] + "] is unknown");
#endif
			unknown_objects.push_back("ext_ids_[i]");
			tmp_ext(3 * i) = INF;
			tmp_ext(3 * i + 1) = INF;
			tmp_ext(3 * i + 2) = INF;
			continue;
		}
		tf::transformEigenToKDL(ps_->getFrameTransform("/" + ext_ids_[i]), ext_w);
		ext_root = root_w.Inverse() * ext_w;
		tmp_ext(3 * i) = ext_root.p.x();
		tmp_ext(3 * i + 1) = ext_root.p.y();
		tmp_ext(3 * i + 2) = ext_root.p.z();
	}
	phi.resize(tmp_joint.rows() + tmp_ext.rows());
	phi << tmp_joint, tmp_ext;
	return true;
}

bool kinematica::KinematicScene::getJacobian(Eigen::MatrixXd & jac)
{
	boost::mutex::scoped_lock(locker_);
	if (!kinematic_solver_.getJacobian(jac))
	{
#ifdef DEBUG_MODE
		KS_INFO("Getting Forward Mapping Failed");
#endif
		return false;
	}
//	uint N=jac.size(),E=ext_ids_.size();
//	jac.resize(N+E,Eigen::NoChange);
//	jac.block(N,0,E,jac.cols())=Eigen::MatrixXd::Zero(E,jac.cols());
	return true;
}

bool kinematica::KinematicScene::updateSceneToKinematica()
{
	boost::mutex::scoped_lock(locker_);
	std::map<std::string, std::string>::iterator it;
	KDL::Frame eef_w, link_w, eef_link;
	kinematica::SolutionForm_t new_solution;
	for (it = eff_world_map_.begin(); it != eff_world_map_.end(); ++it)
	{
		if (!ps_->knowsFrameTransform("/" + it->first))
		{
#ifdef DEBUG_MODE
			KS_INFO("Frame Transform of [" + it->first + "] is unknown");
#endif
			return false;
		}
		if (!ps_->knowsFrameTransform("/" + it->second))
		{
#ifdef DEBUG_MODE
			KS_INFO("Frame Transform of [" + it->second + "] is unknown");
#endif
			//return false;
			//If the transform of the attached object is unknown, we just
			//use old one, rather than return a false
			continue;
		}

		tf::transformEigenToKDL(ps_->getFrameTransform("/" + it->first), link_w);
		tf::transformEigenToKDL(ps_->getFrameTransform("/" + it->second), eef_w);
		eef_link = link_w.Inverse() * eef_w;

		/** Now we update Kinematica offset */
		if (!kinematic_solver_.modifyEndEffector(it->first, eef_link))
		{
#ifdef DEBUG_MODE
			KS_INFO("Update end-effector [" + it->first + "] failed");
#endif
			return false;
		}
	}
	return true;
}

bool kinematica::KinematicScene::getPoses(const std::vector<std::string> names,
		std::vector<KDL::Frame> poses)
{
	boost::mutex::scoped_lock(locker_);
	uint size = names.size(), i;
	poses.resize(size);
	for (i = 0; i < size; i++)
	{
		if (kinematic_solver_.getPose(names[i], poses[i]))
		{
			continue;
		}
		else if (ps_->knowsFrameTransform(names[i]))
		{

			tf::transformEigenToKDL(ps_->getFrameTransform(names[i]), poses[i]);
		}
		else
		{
#ifdef DEBUG_MODE
			KS_INFO("Frame Transform of [" + names[i] + "] is not available");
#endif
			poses.resize(0);
			return false;
		}
	}
	return true;
}

bool kinematica::KinematicScene::addObject(const KS_Object object)
{
	//TODO
	//We might want to add objects into our own scene without affect the original scene
	//but right now, we can just add things through moveit scene
	return true;
}
void kinematica::KinematicScene::startPublishing()
{
#ifdef DEBUG_MODE
	KS_INFO("Start Publishing Robot State To The Scene");
#endif
	boost::mutex::scoped_lock(locker_);
	isPublishing_ = true;
}

void kinematica::KinematicScene::stopPublishing()
{
#ifdef DEBUG_MODE
	KS_INFO("Stop Publishing Robot State To The Scene");
#endif
	boost::mutex::scoped_lock(locker_);
	isPublishing_ = false;
}

void kinematica::KinematicScene::publishScene()
{
	boost::mutex::scoped_lock(locker_);
	if (isPublishing_)
	{
		moveit_msgs::PlanningScene planning_scene;
		ps_->getPlanningSceneMsg(planning_scene);
		scene_pub_.publish(planning_scene);
	}
}

bool kinematica::KinematicScene::updateScene()
{
	boost::mutex::scoped_lock(locker_);
	if (!get_scene_client_.call(get_scene_srv_))
	{
		KS_INFO("Cant get Planning Scene");
		return false;
	}
	ps_->usePlanningSceneMsg(get_scene_srv_.response.scene);
	return true;
}
void kinematica::KinematicScene::sceneCallback(const moveit_msgs::PlanningScene::ConstPtr & scene)
{
#ifdef DEBUG_MODE
	KS_INFO("Setting Planning Scene From Moveit (" + scene->name + ").");
#endif
	boost::mutex::scoped_lock(locker_);
	ps_->setPlanningSceneDiffMsg(*scene);
}
void kinematica::KinematicScene::KS_INFO(std::string info)
{
	std::cout << "Kinematic Scene: " << info << std::endl;
}
