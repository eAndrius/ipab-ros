/*
 * ks_test_node.cpp
 *
 *  Created on: 13 Mar 2014
 *      Author: yiming
 */
#include "k_scene/kinematic_scene.h"

int main(int argc, char **argv)
{
	ros::init(argc, argv, "KS");

	kinematica::KinematicScene KS("KS", "right_arm", "/move_group/monitored_planning_scene");

	kinematica::SolutionForm_t solution;
	solution.root_segment = "base";

	solution.root_seg_off = KDL::Frame::Identity();
	solution.joints_update =
	{	"left_s0", "left_s1", "left_e0", "left_e1", "left_w0", "left_w1", "left_w2",
		"right_s0", "right_s1", "right_e0", "right_e1", "right_w0", "right_w1", "right_w2"};

	solution.zero_other_joints = true;
	solution.ignore_unused_segs = true;
	solution.end_effector_segs =
	{	"left_wrist"};
	solution.end_effector_offs={KDL::Frame::Identity()};
	kinematica::KinematicSceneForm_t ks_form;
	ks_form.optimisation = solution;
	ks_form.eff_world_map["left_wrist"] = "pr2base";
	ks_form.ext_ids.clear();
	ks_form.ext_ids.push_back("box");
	ks_form.ext_ids.push_back("pr2base");
	KS.initKinematicScene("/home/yimingyang/ros_ws/baxter/src/baxter_description/urdf/baxter.urdf", ks_form);
	/**** If you want to test, make sure first add the objects from moveit planning scene, and publish the scene (under Context)****/
	while (ros::ok())
	{
		ros::spinOnce();
		ros::Duration(1).sleep();
		Eigen::VectorXd q(14);
		q<<0,0,0,0,0,0,0,0,0,0,0,0,0,0;
		KS.update(q);
		Eigen::VectorXd t;
		Eigen::MatrixXd jac;
		std::vector<std::string> uo;
		if (KS.getForwardMap(t,uo)&& KS.getJacobian(jac))
		{
			std::cout << "OUTPUT: " << t.transpose() << std::endl;
			std::cout<<"Jac: "<<jac<<std::endl;
		}
		else
			std::cout << "Get Task Output failed" << std::endl;
	}
	return 0;
}

