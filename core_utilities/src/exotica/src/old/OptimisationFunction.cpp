#include "exotica/OptimisationFunction.h"

exotica::OptimisationFunction::OptimisationFunction()
{
  name_ = "top-level";    //!< Default Name
}


exotica::OptimisationFunction::~OptimisationFunction()
{
  //!< For now empty destructor since shared_ptrs take care of all de-allocation automatically
}


void exotica::OptimisationFunction::setParams(const std::string * const optimisation_name, const Eigen::MatrixXd * const task_solut_weights)
{
  if (optimisation_name != NULL) {  name_ = *optimisation_name; }
  if (task_solut_weights != NULL)
  {
    configuration_weights_ = *task_solut_weights;
  }
}


std::string exotica::OptimisationFunction::getName()
{
  return name_;
}


bool exotica::OptimisationFunction::addTask(std::string task_type, std::string task_name, const OptimisationParameters_t & params)
{
#ifdef EX_CHECKS
  if (task_list_.find(task_name) != task_list_.end()) { return false; } //!< If exists already, fail
#endif
  boost::shared_ptr<TaskDefinition> temp_ptr = TaskCreator::Instance()->createObject(task_type, params);
  
  if (temp_ptr == nullptr)  { return false; }       //!< If creation was unsuccessful
  task_list_[task_name] = temp_ptr;
  
  return true;
}


bool exotica::OptimisationFunction::deleteTask(std::string task_name)
{
  if (task_list_.erase(task_name))  { return true; }
  else                              { return false;}
}


boost::weak_ptr<exotica::TaskDefinition> exotica::OptimisationFunction::access(std::string task_name)
{
  boost::weak_ptr<TaskDefinition> temp_ptr;
  if (task_list_.find(task_name) != task_list_.end()) //!< If found
  {
    temp_ptr = task_list_[task_name];     //!< Assign from shared pointer
  }
  return temp_ptr;
}


bool exotica::OptimisationFunction::updateState(const Eigen::VectorXd & configuration, int index)
{
  //!< Checks
#ifdef EX_CHECKS
  if (configuration.size() != configuration_weights_.rows()) { return false; } //!< Already a mismatch between the weighting and the given configuration
#endif
  
  //!< Temporaries;
  bool success = true;
  
  //!< Update Calls
  for(Task_Map_t::iterator it=task_list_.begin(); it!=task_list_.end() && success; ++it)
  {
    success = it->second->updateTask(configuration, index);
  }
  
  return success;
}


bool exotica::OptimisationFunction::getJacobian(Eigen::MatrixXd & big_jacobian, int index)
{  
  int task_size = 0;
  for(Task_Map_t::iterator it=task_list_.begin(); it!=task_list_.end(); ++it)
  {
    task_size += it->second->getTaskDimension(index);  //!< Add the dimensionality
  }
  
  //!< Now Create the Jacobian
  big_jacobian.resize(task_size, configuration_weights_.rows());  //!< First allocate the memory accordingly
  Eigen::MatrixXd temp_jacobian;                //!< Temporary Jacobian
  int curr_rows = 0;                            //!< For managing the blocks of the Jacobian
  for(Task_Map_t::iterator it=task_list_.begin(); it!=task_list_.end(); ++it)
  {
    Eigen::MatrixXd temp_jacobian = it->second->getJacobian(index);
#ifdef EX_CHECKS
    if (temp_jacobian.cols() != configuration_weights_.rows()) { return false; } //!< Something went wrong if the cols != number of configuration variables
#endif
    big_jacobian.block(curr_rows, 0, temp_jacobian.rows(), temp_jacobian.cols()) = temp_jacobian; //!< Copy the Jacobian
    curr_rows += temp_jacobian.rows();
  }
  
  return true; //!< IF made it this far..
}


bool exotica::OptimisationFunction::getTaskError(Eigen::VectorXd & task_errors, bool & within_tolerance, int index)
{   
  //!< Check dimensionality
  int task_size = 0;
  for(Task_Map_t::iterator it=task_list_.begin(); it!=task_list_.end(); ++it)
  {
    task_size += it->second->getTaskDimension(index);
  }
    
  //!< Now prepare the task_error variable  
  task_errors.resize(task_size);  //!< Resize to correct dimensions
  int curr_index = 0;
  within_tolerance = true;
  for(Task_Map_t::iterator it=task_list_.begin(); it!=task_list_.end(); ++it)
  {
    Eigen::VectorXd temp_task = it->second->getPhi(index);
#ifdef EX_CHECKS
    if (temp_task.rows() != it->second->getTaskDimension(index)) { return false; } //!< Something went wrong if the rows != dimension of the task-space
#endif
    temp_task = it->second->getGoal(index) - temp_task;
    task_errors.segment(curr_index, temp_task.size()) = temp_task;
    if (temp_task.norm() > it->second->getTolerance(index))  { within_tolerance = false; }//!< IF mean above tolerance for one of the tasks then fail
    curr_index += temp_task.size();
  }
  
  return true; //!< If made it this far!
}


bool exotica::OptimisationFunction::getWeights(Eigen::MatrixXd & config_weighting, Eigen::MatrixXd & task_weighting, int index)
{
  //!< First check dimensionality
  int task_size = 0;
  for(Task_Map_t::iterator it=task_list_.begin(); it!=task_list_.end(); ++it)
  {
    task_size += it->second->getTaskDimension(index);
  }
  
  //!< Now create the Task-Space Weighting Matrix
  task_weighting = Eigen::MatrixXd::Zero(task_size, task_size);
  int current_index = 0;
  for(Task_Map_t::iterator it=task_list_.begin(); it!=task_list_.end(); ++it)
  {
    Eigen::MatrixXd current_weights = it->second->getGoalWeights(index) * it->second->getTaskWeight(index); //!< Get the Matrix of goal weights
    task_weighting.block(current_index, current_index, current_weights.rows(), current_weights.cols()) = current_weights; //!< Assign Matrix
    current_index += current_weights.rows();  //!< In preparation for next iteration
  }
  
  //!< Finally just copy the Config-Space Weighting
  config_weighting = configuration_weights_;
  
  return true;
  
}


int exotica::OptimisationFunction::getConfigurationDimension()
{
  return configuration_weights_.rows(); //!< Should be a square matrix...
}
