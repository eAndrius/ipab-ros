#include "exotica/VelocitySolver.h"

exotica::VelocitySolver::VelocitySolver(const OptimisationParameters_t & params) : optimisation_params_(params)
{
   //!< Nothing to initialise
}

exotica::VelocitySolver::~VelocitySolver()
{
  //std::cout << "Calling Destructor of Velocity Solver" << std::endl;
}

bool exotica::VelocitySolver::solve(std::vector<OptimisationFunction> & problem, Eigen::VectorXd & velocity, int index)
{
  Eigen::MatrixXd null_space_map;               //!< Stores the Null-Space mapping from one level of optimisation to the next
  Eigen::MatrixXd config_weights, task_weights; //!< Weight Matrices
  Eigen::MatrixXd big_jacobian, inv_jacobian;   //!< Jacobian and its pseudo-Inverse
  Eigen::VectorXd task_error;                   //!< Task Error vector for the current optimisation level
  
  velocity = Eigen::VectorXd::Zero(problem[0].getConfigurationDimension());
  if (velocity.size() == 0) {return false; } //!< Not initialised correctly!
  
  null_space_map = Eigen::MatrixXd::Identity(velocity.size(), velocity.size()); //!< For the first one it is the identity
  for (int i=0; i<problem.size(); i++) //!< Iterate through all levels of the optimisation stack
  {
    bool temp_tolerance;
    if (!problem[i].getWeights(config_weights, task_weights, index)) {return false; } //!< Get weightings and return false if unsuccessful
    if (!problem[i].getJacobian(big_jacobian, index)) {return false; }//!< Again guard against failure
    if (!problem[i].getTaskError(task_error, temp_tolerance, index)) {return false; }
    if (!getInverse(big_jacobian, config_weights, task_weights, inv_jacobian)) {return false; }
    velocity = velocity + null_space_map * inv_jacobian * task_error;
    //std::cout << "------ \n" << velocity << "\n------" << std::endl;
    null_space_map = null_space_map * (Eigen::MatrixXd::Identity(velocity.size(), velocity.size()) - inv_jacobian * big_jacobian); //!< Cast into the next null-space
  }
  
  return true;  //!< If made it this far...
}


bool exotica::VelocitySolver::initBase(tinyxml2::XMLHandle & base_element)
{
  //!< Currently, base velocity solver has no common functionality
  
  return initDerived(base_element);
}
