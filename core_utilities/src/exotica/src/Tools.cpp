#include "exotica/Tools.h"

bool exotica::ok(const EReturn & value)
{
  return (value == SUCCESS or value == WARNING);
}

exotica::EReturn exotica::getMatrix(const tinyxml2::XMLElement & xml_matrix, Eigen::MatrixXd & eigen_matrix)
{
  int dimension = 0;
  CHECK_EXECUTION;
  
  if (xml_matrix.QueryIntAttribute("dim", &dimension) != tinyxml2::XML_NO_ERROR)
  {
    INDICATE_FAILURE;
    eigen_matrix = Eigen::MatrixXd(); //!< Null matrix again
    return PAR_ERR;
  }
  CHECK_EXECUTION;
  
  if (dimension < 1)
  {
    INDICATE_FAILURE;
    eigen_matrix = Eigen::MatrixXd(); //!< Null matrix again
    return PAR_ERR;
  }
  eigen_matrix.resize(dimension, dimension);
  CHECK_EXECUTION;
  
  if (!xml_matrix.GetText())
  {
    INDICATE_FAILURE;
    eigen_matrix = Eigen::MatrixXd(); //!< Null matrix again
    return PAR_ERR;
  }
  
  std::istringstream text_parser(xml_matrix.GetText());
  double temp_entry;
  for (int i=0; i<dimension; i++) //!< Note that this does not handle comments!
  {
    CHECK_EXECUTION;
    for (int j=0; j<dimension; j++)
    {
      CHECK_EXECUTION;
      text_parser >> temp_entry;
      if (text_parser.fail() || text_parser.bad())  //!< If a failure other than end of file
      {
        INDICATE_FAILURE;
        eigen_matrix.resize(0,0);
        return PAR_ERR;
      }
      else
      {
        eigen_matrix(i,j) = temp_entry;
        CHECK_EXECUTION;
      }
    }
  }
  
  return SUCCESS;
}

exotica::EReturn exotica::getVector(const tinyxml2::XMLElement & xml_vector, Eigen::VectorXd & eigen_vector)
{
  //!< Temporaries
  double temp_entry;
  int i = 0;
  
  if (!xml_vector.GetText())
  {
    INDICATE_FAILURE;
    eigen_vector = Eigen::VectorXd(); //!< Null matrix again
    return PAR_ERR;
  }
  std::istringstream text_parser(xml_vector.GetText());
  
  //!< Initialise looping
  text_parser >> temp_entry;
  while (!(text_parser.fail() || text_parser.bad()))  //!< No commenting!
  {
    eigen_vector.conservativeResize(++i); //!< Allocate storage for this entry (by increasing i)
    eigen_vector(i-1) = temp_entry;
    text_parser >> temp_entry;
  }
  return (i > 0) ? SUCCESS : PAR_ERR;
}


exotica::EReturn exotica::getDouble(const tinyxml2::XMLElement & xml_value, double & double_value)
{  
  if (!xml_value.GetText())
  {
    INDICATE_FAILURE;
    return PAR_ERR;
  }
  std::istringstream text_parser(xml_value.GetText());
  
  text_parser >> double_value;
  if (!(text_parser.fail() || text_parser.bad()))
  {
    return SUCCESS;
  }
  else
  {
    return PAR_ERR;
  }
}
