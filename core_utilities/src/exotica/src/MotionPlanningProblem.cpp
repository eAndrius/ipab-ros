#include "exotica/MotionPlanningProblem.h"

exotica::MotionPlanningProblem::MotionPlanningProblem()
{
  //!< Empty Constructor
}

exotica::EReturn exotica::MotionPlanningProblem::initBase(tinyxml2::XMLHandle & handle)
{
  //!< Temporaries
  tinyxml2::XMLHandle                           xml_handle(handle);
  EReturn                                       ret_value = SUCCESS;
  int                                           count;
  std::map<std::string, TaskMapPointer>         map_list;
  std::map<std::string, TaskDefinitionPointer>  task_list;
  
  //!< Refresh
  task_maps_.clear();
  task_defs_.clear();
  prob_spec_.clear();
  
  //!< First we will create the maps
  xml_handle = handle.FirstChildElement("Map");
  count = 0;
  while (xml_handle.ToElement() and ok(ret_value))  //!< While we are still in a valid situation
  {
    std::string name (xml_handle.ToElement()->Attribute("name"));
    if (name.empty()) { INDICATE_FAILURE; ret_value = PAR_ERR; break; }
    if (map_list.find(name) != map_list.end()) { INDICATE_FAILURE; ret_value = PAR_ERR; break; }
    
    std::string type (xml_handle.ToElement()->Attribute("type"));
    if (type.empty()) { INDICATE_FAILURE; ret_value = PAR_ERR; break; }
    
    TaskMapPointer temp_ptr;
    ret_value = TaskMapFactory.createObject(type, temp_ptr);
    
    if (ok(ret_value))
    {
      map_list[name] = temp_ptr;  //!< Copy the shared_ptr;
      task_maps_.push_back(temp_ptr);
      count++;
      ret_value = temp_ptr->initBase(xml_handle);
    }
    xml_handle = xml_handle.NextSiblingElemen("Map");
  }
  
  //!< No maps defined (some tests)
  if (count < 1)  
  {
    ret_value = WARNING;
  }
  if (!ok(ret_value))
  {
    INDICATE_FAILURE;
    return PAR_ERR;
  }
  
  //!< Now the Task Definitions (all)
  xml_handle = handle.FirstChildElement("Task");
  count = 0;
  while (xml_handle.ToElement() and ok(ret_value))
  {
    std::string name (xml_handle.ToElement()->Attribute("name"));
    if (name.empty()) { INDICATE_FAILURE; ret_value = PAR_ERR; break; }
    if (task_list.find(name) != task_list.end()) { INDICATE_FAILURE; ret_value = PAR_ERR; break; }
    
    tinyxml2::XMLHandle xml_child(xml_handle.FirstChildElement("Map"));
    
  }
}

exotica::EReturn exotica::MotionPlanningProblem::updateAll(const Eigen::VectorXd & x)
{
  EReturn to_return = SUCCESS;
  
  for (int i=0; i<task_maps_.size() and (to_return == SUCCESS or to_return == WARNING); i++)
  {
    EReturn temp_return = task_maps_[i].update(x);
    if (temp_return)
    {
      to_return = temp_return;  // Whether to_return was successful or just warning, now must set to temp_return which can be just a warning or a failure
    }      
  }

  return to_return;
}
