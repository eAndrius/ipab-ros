#include "exotica/TaskMap.h"

exotica::TaskMap::TaskMap()
{
  //!< Just reset the flags
  phi_ok_ = false;
  jac_ok_ = false;
}

exotica::EReturn exotica::TaskMap::initBase(tinyxml2::XMLHandle & handle)
{
  invalidate();  //!< Clear flags
  return initDerived(handle); //!< Call the derived member
}

exotica::EReturn exotica::TaskMap::phi(Eigen::VectorXd & y)
{
  LOCK(phi_lock_);  //!< Synchronisation
  
  if (phi_ok_)
  {
    CHECK_EXECUTION;
    y = phi_;
    return SUCCESS;
  }
  else
  {
    INDICATE_FAILURE;
    return MMB_NIN;
  }
}

exotica::EReturn exotica::TaskMap::jacobian(Eigen::MatrixXd & J)
{
  LOCK(jac_lock_);
  
  if (jac_ok_)
  {
    CHECK_EXECUTION;
    J = jac_;
    return SUCCESS;
  }
  else
  {
    INDICATE_FAILURE;
    return MMB_NIN;
  }
}

exotica::EReturn exotica::TaskMap::setPhi(const Eigen::VectorXd & y)
{
  LOCK(phi_lock_);
  
  phi_ = y;
  phi_ok_ = true;
  return SUCCESS;
}

exotica::EReturn exotica::TaskMap::setJacobian(const Eigen::MatrixXd & J)
{
  LOCK(jac_lock_);
  
  jac_ = J;
  jac_ok_ = true;
  return SUCCESS;
}

void exotica::TaskMap::invalidate()
{
  LOCK(phi_lock_);
  LOCK(jac_lock_);
  phi_ok_ = jac_ok_ = false;
}
