#include "exotica/MotionPlanningComponent.h"

exotica::MotionPlanningComponent::MotionPlanningComponent()
{
  //!< Empty constructor
}

exotica::EReturn exotica::MotionPlanningComponent::initBase(tinyxml2::XMLHandle & handle, const std::map<std::string, boost::shared_ptr<TaskDefinition> > & task_def_map)
{
  return initDerived(handle, task_def_map);
}
