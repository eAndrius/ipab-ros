#include "exotica/TaskDefinition.h"

exotica::TaskDefinition::TaskDefinition()
{
  //! Empty Constructor...
}

exotica::EReturn exotica::TaskDefinition::initBase(tinyxml2::XMLHandle & handle)
{
  return initDerived(handle);
}

exotica::EReturn exotica::TaskDefinition::setTaskMap(const boost::shared_ptr<TaskMap> & task_map)
{
  LOCK(map_lock_);
  task_map_ = task_map;
  return SUCCESS;
}

exotica::EReturn exotica::TaskDefinition::phi(Eigen::VectorXd & y)
{
  LOCK(map_lock_);
  if (task_map_ != nullptr)
  {
    CHECK_EXECUTION;
    return task_map_->phi(y);
  }
  else
  {
    INDICATE_FAILURE;
    return MEM_ERR;
  }
}

exotica::EReturn exotica::TaskDefinition::jacobian(Eigen::MatrixXd & J)
{
  LOCK(map_lock_);
  if (task_map_ != nullptr)
  {
    CHECK_EXECUTION;
    return task_map_->jacobian(J);
  }
  else
  {
    INDICATE_FAILURE;
    return MEM_ERR;
  }
}

