#include "exotica/PositionSolver.h"

exotica::PositionSolver::PositionSolver(const OptimisationParameters_t & params)
{
  boost::mutex::scoped_lock(member_lock_);
  optimisation_params_ = params;
  truncate_vel_ = (params.max_step > 0.0);
}

exotica::PositionSolver::~PositionSolver()
{
  //!< Empty Destructor
}

exotica::OptimisationParameters_t exotica::PositionSolver::getParams(void)
{
  boost::mutex::scoped_lock(member_lock); //!< Lock for synchronisation
  return optimisation_params_;            //!< Return
}

bool exotica::PositionSolver::scaleVelocity(Eigen::VectorXd & velocity) const
{
  boost::mutex::scoped_lock(member_lock_);
  if (!truncate_vel_) { return true; } //!< Then do not modify
  //else
  double max_value = velocity.cwiseAbs().maxCoeff();  //!< Find the maximum value
  //std::cout << max_value << std::endl;
  if (max_value > optimisation_params_.max_step)
  {
    //std::cout << "Greater indeed!" << std::endl;
    max_value = optimisation_params_.max_step/max_value;
    velocity *= max_value;
  }
  //std::cout << "------ \n" << velocity << "\n------" << std::endl;
  return true;
}
