/***********************************************************************\
|    TaskMap defines the forward mapping for tasks (basically the      |
|   phi-function definition and its derivatives). It is an abstract     |
|   class. NOT Entirely Thread-safe.                                    |
|                                                                       |
|           Developer: Michael Camilleri (mcamnadur@gmail.com)          |
|                    Last Edited: 12 - March - 2014                     |
\***********************************************************************/

#ifndef EXOTICA_TASK_MAP_H
#define EXOTICA_TASK_MAP_H

#include "exotica/Object.h"       //!< The EXOTica base class
#include "exotica/Factory.h"      //!< The Factory template
#include "exotica/Test.h"         //!< The Test factory template
#include <Eigen/Dense>            //!< Generally dense manipulations should be enough
#include <boost/thread/mutex.hpp> //!< The boost thread-library for synchronisation
#include <string>

/**
 * \brief Convenience registrar for the TaskMap Type
 */
#define REGISTER_TASKMAP_TYPE(TYPE, DERIV) EXOTICA_REGISTER(std::string, exotica::TaskMap, TYPE, DERIV)

namespace exotica
{
  class TaskMap : public Object
  {
    public:
      /**
       * \brief Default Constructor
       */
      TaskMap();
      
      /**
       * \brief Initialiser (from XML)
       * @param handle  The handle to the XML-element describing the task map
       * @return        Indication of success/failure: Currently returns the indicator from calling the initDerived() function.
       */
      EReturn initBase(tinyxml2::XMLHandle & handle);
      
      /**
       * \brief Updates the output function of the task: PURE-VIRTUAL
       * @pre       Should call refresh()
       * @post      Should store the results using setY() and setYDot() if successful
       * @param  x  The State-space vector for the robot
       * @return    Should indicate success/otherwise using the Exotica error types
       */
      virtual EReturn update(const Eigen::VectorXd & x) = 0;
      
      /**
       * \brief Getter for the task-space vector
       * @param  y  Placeholder for storing the task-space vector
       * @return    Indication of success or otherwise: SUCCESS if ok
       *                                                MMB_NIN if the phi-vector is not correctly defined
       */
      EReturn phi(Eigen::VectorXd & y);
      
      /**
       * \brief Getter for the task-space velocity matrix
       * @param  ydot Placeholder for the Jacobian
       * @return      Indication of success or otherwise: SUCCESS if ok
       *                                                  MMB_NIN if the jacobian is not correctly computed
       */
      EReturn jacobian(Eigen::MatrixXd & J);
      
    protected:
      /**
       * \brief Setter for the Task-space mapping: enforces thread-safety
       * @param  y  The task-space vector
       * @return    Always returns SUCCESS
       */
      EReturn setPhi(const Eigen::VectorXd & y);
      
      /**
       * \brief Setter for the Task-space Velocity (Jacobian): enforces thread-safety
       * @param  ydot  The task-space Jacobian
       * @return       Always returns SUCCESS
       */
      EReturn setJacobian(const Eigen::MatrixXd & J);
      
      /**
       * \brief Invalidates the phi and jacobian matrices: does not de-allocate memory!
       */
      void invalidate();
      
      /**
       * \brief Initialises members of the derived type: PURE_VIRTUAL
       * @param handle  The handle to the XML-element describing the task
       * @return        Should indicate success/failure
       */
      virtual EReturn initDerived(tinyxml2::XMLHandle & handle) = 0;
      
    private:
      /**
       * \brief Private data members for information hiding and thread-safety
       */
      Eigen::VectorXd   phi_;       //!< The Task-space co-ordinates
      Eigen::MatrixXd   jac_;       //!< The Jacobian matrix
      bool              phi_ok_;    //!< Indicates that the phi matrix contains valid data
      bool              jac_ok_;    //!< Indicates that the jacobian contains valid data
      boost::mutex      phi_lock_;  //!< Lock around the Y-Vector
      boost::mutex      jac_lock_;  //!< Lock around the Jacobian
  };
 
  //!< Typedefines for some common functionality
  typedef Factory<std::string, TaskMap> TaskMapCreator;
  typedef boost::shared_ptr<TaskMap>    TaskMapPointer;
}
#endif
