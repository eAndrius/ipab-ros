/***********************************************************************\
|    Object is the base class from which all members of the exotica     |
|   library should inherit. It currently provides functionality for     |
|   run-time type identification, through C++ RTTI constructs. It is    |
|   currently a header-only implementation.                             |
|                                                                       |
|           Developer: Michael Camilleri (mcamnadur@gmail.com)          |
|                    Last Edited: 12 - March - 2014                     |
\***********************************************************************/

#ifndef EXOTICA_OBJECT_H
#define EXOTICA_OBJECT_H

#include <typeinfo> //!< The RTTI Functionality of C++
#include <cxxabi.h> //!< The demangler for gcc... this makes this system dependent!
#include <string>   //!< C++ type strings

namespace exotica
{
  class Object
  {
    public:
      /**
       * \brief Constructor: default
       */
      Object()
      {
        //!< Empty constructor
      };
      
      /**
       * \brief Type Information wrapper: must be virtual so that it is polymorphic...
       * @return String containing the type of the object
       */
      inline virtual std::string type()
      {
        int         status;
        std::string name;
        char *      temp; //!< We need to store this to free the memory!
        
        temp = abi::__cxa_demangle(typeid(*this).name(), 0, 0, &status);  
        name = std::string(temp);
        free(temp);
        return name;
      };      
  };
}
#endif
