/***********************************************************************\
|    This class defines the over-arching problem specification for      |
|  EXOTica. Specialisations are introduced through the child MPC's      |
|  (MotionPlanningComponent's). It instantiates and manages all 
|                                                                       |
|           Developer: Michael Camilleri (mcamnadur@gmail.com)          |
|                    Last Edited: 24 - March - 2014                     |
\***********************************************************************/
#ifndef EXOTICA_MOTION_PLANNING_PROBLEM_H
#define EXOTICA_MOTION_PLANNING_PROBLEM_H

#include "exotica/Object.h"
#include "exotica/TaskMap.h"
#include "exotica/TaskDefinition.h"
#include "exotica/MotionPlanningComponent.h"
#include "tinyxml2/tinyxml2.h"
#include <map>

namespace exotica
{

  class MotionPlanningProblem : public Object
  {
    public:
      /**
       * \brief Default Constructor
       */
      MotionPlanningProblem();
      
      /**
       * \brief Initialiser (from XML)
       * @param handle[in] The handle to the XML-element describing the Problem Definition
       * @return           Indication of success/failure: TODO
       */
      EReturn initBase(tinyxml2::XMLHandle & handle);
      
      /**
       * \brief Wraps up the updating of all task-maps at one go.
       * @param x The Configuration (state) variable
       * @return  SUCCESS if all are updated correctly, else the respective failure condition for the first map that fails.
       */
      EReturn updateAll(const Eigen::VectorXd & x);
            
      /*** (Public) Class Members ***/
      std::vector< TaskMapPointer >         task_maps_; //!< The List of TaskMap definitions
      std::vector< TaskDefinitionPointer >  task_defs_; //!< The (Full) List of TaskDefinition objects
      std::map<std::string, MPCPointer >    prob_spec_; //!< Problem Specialisations (such as AICO, RRT, LQR etc...)
  };
}
#endif
