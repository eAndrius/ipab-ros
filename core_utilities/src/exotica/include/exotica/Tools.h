/***********************************************************************\
|    This component provides a set of common functionalities for        |
|   EXOTica:                                                            |
|     1) Debugging macros                                               |
|     2) Enum for error-propagation/reporting                           |
|     3) XML-Parsing functionality                                      |
|                                                                       |
|           Developer: Michael Camilleri (mcamnadur@gmail.com)          |
|                    Last Edited: 14 - March - 2014                     |
\***********************************************************************/

#ifndef EXOTICA_TOOLS_H
#define EXOTICA_TOOLS_H

#include "tinyxml2/tinyxml2.h"
#include <Eigen/Dense>
#include <iostream>

  /**
   * \brief A set of debugging tools: basically these provide easy ways of checking code execution through std::cout prints
   */
  #ifndef __PRETTY_FUNCTION__
    #define __PRETTY_FUNCTION__ __func__
  #endif

  #ifdef EXOTICA_DEBUG_MODE
    #define CHECK_EXECUTION         std::cout << "Ok in " << __FILE__ << " at line " << __LINE__ << " within function " << __PRETTY_FUNCTION__ << "." << std::endl  //!< With endline
    #define INDICATE_FAILURE        std::cout << "Failed in " << __FILE__ << " at line " << __LINE__ << " within function " << __PRETTY_FUNCTION__ << "." << std::endl  //!< With endline
  #else
    #define CHECK_EXECUTION   //!< No operation
    #define INDICATE_FAILURE  //!< No operation
  #endif
  
  /**
   * \brief A convenience macro for the boost scoped lock
   */
  #define LOCK(x) boost::mutex::scoped_lock(x)
  
  /**
   * \brief A double-wrapper MACRO functionality for generating unique object names: The actual functionality is provided by EX_UNIQ (for 'exotica unique')
   */
  #define EX_CONC(x, y) x ## y
  #define EX_UNIQ(x, y) EX_CONC(x, y)

namespace exotica
{
  /**
   * \brief Enum for error reporting throughout the library
   */
  enum EReturn
  {
    SUCCESS = 0,  //!< Indicates successful execution of function
    PAR_INV = 1,  //!< Invalid Parameter TYPE! (when using dynamic polymorphism)
    PAR_ERR = 2,  //!< Uninitialised or incorrect parameter value (could be sizes of vectors, nan etc...)
    MMB_NIN = 3,  //!< A member required by this function is Not INititialised correctly
    MEM_ERR = 4,  //!< A memory error (for example when creating a new class)
    WARNING = 50, //!< A generic warning:
    FAILURE = 100 //!< Indicates a generic failure  
  };
  
  bool ok(const EReturn & value);
  
  /**
   * \brief Enum for terminating conditions
   *        The way in which the values are defined is such that simple arithmetic can be used to check termination: if the sum exceeds or equals the number of soft criteria (whether because they 
   *        are all satisfied, or because there is one hard constraint) then one should stop.
   */
  enum ETerminate
  {
    CONTINUE = 0,     //!< Continue iteration (terminating condition not met)
    SOFT_END = 1,     //!< Soft ending: this means that its contribution will be 'anded' with other soft-ends
    HARD_END = 999    //!< Indicate that the iteration should terminate no matter what  
  };
  
  /**
   * \brief Parses an XML element into an Eigen Matrix. The handle must point directly to the element with the matrix as its text child and must have no comments!
   * @param xml_matrix    The element for the XML matrix
   * @param eigen_matrix  Placeholder for storing the parsed matrix
   * @return              Indication of success: TODO
   */
  EReturn getMatrix(const tinyxml2::XMLElement & xml_matrix, Eigen::MatrixXd & eigen_matrix);
  
  EReturn getVector(const tinyxml2::XMLElement & xml_vector, Eigen::VectorXd & eigen_vector);
  
  EReturn getDouble(const tinyxml2::XMLElement & xml_value, double & double_value);
}
#endif
