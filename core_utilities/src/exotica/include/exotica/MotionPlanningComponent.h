/***********************************************************************\
|    The base class for all Specialisations of Problem Definitions      |
|  as required by different type of solvers. Because each problem       |
|  might require a different structure, the TaskDefinitions are not     |
|  kept within the base class, but a map of TaskDefinition pointers     |
|  is made available to the initDerived function.                       |
|                                                                       |
|           Developer: Michael Camilleri (mcamnadur@gmail.com)          |
|                    Last Edited: 24 - March - 2014                     |
\***********************************************************************/
#ifndef EXOTICA_MOTION_PLANNING_COMPONENT_H
#define EXOTICA_MOTION_PLANNING_COMPONENT_H

#include "exotica/TaskDefinition.h"
#include "tinyxml2/tinyxml2.h"
#include "exotica/Object.h"
#include <string>
#include <vector>
#include <map>

#define REGISTER_PLANNINGCOMPONENT_TYPE(TYPE, DERIV) EXOTICA_REGISTER(std::string, exotica::MotionPlanningComponent, TYPE, DERIV)

namespace exotica
{
  class MotionPlanningComponent : public Object
  {
    public:
      /**
       * \brief Default Constructor
       */
      MotionPlanningComponent();
      
       /**
       * \brief Initialiser (from XML)
       * @param handle[in]        The handle to the XML-element describing the Problem Specialisation
       * @param task_def_map[in]  The map for all TaskDefinition objects (indexed by name)
       * @return                  Currently just returns the result of initDerived()
       */
      EReturn initBase(tinyxml2::XMLHandle & handle, const std::map<std::string, boost::shared_ptr<TaskDefinition> > & task_def_map);
      
    protected:
      /**
       * \brief Initialises members of the derived type: PURE_VIRTUAL
       * @param handle[in]        The handle to the XML-element describing the Problem specialisation
       * @param task_def_map[in]  The map for all TaskDefinition objects (indexed by name)
       * @return                  Should indicate success/failure
       */
      virtual EReturn initDerived(tinyxml2::XMLHandle & handle, const std::map<std::string, boost::shared_ptr<TaskDefinition> > & task_def_map) = 0;
  };
  
  typedef Factory<std::string, MotionPlanningComponent> MPCCreator;
  typedef boost::shared_ptr<MotionPlanningComponent>    MPCPointer;
}
#endif
