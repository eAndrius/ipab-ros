/***********************************************************************\
|    This is the base class for all forms of cost-function type         |
|  implementations. It currently provides no functionality apart from   |
|  allowing polymorphic behaviour between the different cost            |
|  components. THREAD-SAFE                                              |
|                                                                       |
|           Developer: Michael Camilleri (mcamnadur@gmail.com)          |
|                    Last Edited: 18 - March - 2014                     |
\***********************************************************************/

#ifndef EXOTICA_TASK_DEFINITION_H
#define EXOTICA_TASK_DEFINITION_H

#include "exotica/Object.h"       //! The EXOTica base class
#include "exotica/TaskMap.h"      //! The Task map (since we require a ptr to it)
#include "exotica/Tools.h"

#define REGISTER_TASKDEFINITION_TYPE(TYPE, DERIV) EXOTICA_REGISTER(std::string, exotica::TaskDefinition, TYPE, DERIV)

namespace exotica
{
  class TaskDefinition : public Object
  {
    public:
      /**
       * \brief Default Constructor
       */
      TaskDefinition();
      
      /**
       * \brief Base Initialiser 
       * @param handle Handle to the XML Element
       * @return       The result of calling the initDerived() function
       */
      EReturn initBase(tinyxml2::XMLHandle & handle);
      
      /**
       * \brief Set the task map
       * @param task_map  Shared pointer to the task-map
       * @return          Always SUCCESS
       */
      EReturn setTaskMap(const boost::shared_ptr<TaskMap> & task_map);
      
      /**
       * \brief Wrapper for the underlying phi
       * @param  y  Placeholder for storing the task-space vector
       * @return    Indication of success or otherwise: SUCCESS if ok
       *                                                MEM_ERR if the Pointer fails to dereference
       *                                                MMB_NIN if the phi-vector is not correctly defined
       */
      EReturn phi(Eigen::VectorXd & y);
      
      /**
       * \brief Wrapper around the task-space velocity matrix
       * @param  J Placeholder for the Jacobian
       * @return      Indication of success or otherwise: SUCCESS if ok
       *                                                  MEM_ERR if pointer is incorrect
       *                                                  MMB_NIN if the jacobian is not correctly computed
       */
      EReturn jacobian(Eigen::MatrixXd & J);
      
    protected:
      /**
       * \brief Initialises members of the derived type: PURE_VIRTUAL
       * @param handle  The handle to the XML-element describing the ErrorFunction Function
       * @return        Should indicate success/failure
       */
      virtual EReturn initDerived(tinyxml2::XMLHandle & handle) = 0;
      
    private:
      boost::shared_ptr<TaskMap>  task_map_;  //!< Shared pointer to a Task Map from which it gets its inputs
      boost::mutex                map_lock_;  //!< Mapping Lock for synchronisation
  };
  
  typedef Factory<std::string, TaskDefinition> TaskDefinitionCreator;
  typedef boost::shared_ptr<TaskDefinition>    TaskDefinitionPointer;
}
#endif
