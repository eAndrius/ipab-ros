#ifndef EXOTICA_HPP_GENERAL
#define EXOTICA_HPP_GENERAL

//!< Core Library
#include <exotica/Object.h>
#include <exotica/TaskDefinition.h>
#include <exotica/TaskMap.h>
#include <exotica/Factory.h>
#include <exotica/Test.h>
#include <exotica/Tools.h>
#include <exotica/MotionPlanningProblem.h>
#include <exotica/MotionPlanningComponent.h>

//!< The XML Parser
#include <tinyxml2/tinyxml2.h>


#endif
