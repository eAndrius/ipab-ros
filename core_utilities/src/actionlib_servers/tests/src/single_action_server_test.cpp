#include <gtest/gtest.h>
#include <actionlib_servers/action_server_interface.h>
#include <actionlib_servers/single_action_server.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/TestAction.h>

using actionlib_servers::ActionServerInterface;
using actionlib_servers::SingleActionServer;

void executeCallback(float block_time,
                     ActionServerInterface<actionlib::TestAction>* action_server,
                     const ActionServerInterface<actionlib::TestAction>::GoalConstPtr& goal)
{
    // just verify that the server object isn't null
    ASSERT_TRUE(action_server != NULL); // this won't stop the test but it will stop the function
    
    // block for the specified amount of time
    ros::Rate loop_rate(10);
    ros::Duration block_duration(block_time);
    ros::Time start_time = ros::Time::now();
    while(action_server->ok() && (ros::Time::now() - start_time) < block_duration)
    {
        loop_rate.sleep();
    }

    // set the result to success
    if(action_server->ok())
        action_server->setSucceeded();
    else
        action_server->setAborted();
}

void noFinalStateExecuteCallback(ActionServerInterface<actionlib::TestAction>* action_server,
                                 const ActionServerInterface<actionlib::TestAction>::GoalConstPtr& goal)
{
    ASSERT_TRUE(action_server != NULL);
}

bool goalCallback(bool result,
                  const ActionServerInterface<actionlib::TestAction>::GoalConstPtr& goal)
{
    return result;
}


TEST(SingleGoal, GoalSuccess)
{
    // initialize the action server
    SingleActionServer<actionlib::TestAction> action_server("test_action",
                                                            boost::bind(executeCallback, 0.1, _1, _2));

    actionlib::SimpleActionClient<actionlib::TestAction> action_client("test_action", true);

    ros::AsyncSpinner spinner(1);
    spinner.start();

    // wait up to 2 seconds for the action_client server
    bool server_exists = action_client.waitForServer(ros::Duration(1));
    ASSERT_TRUE(server_exists);

    actionlib::TestGoal goal;
    goal.goal = 42; // The answer to life, the universe, and everything
    action_client.sendGoal(goal);

    // wait for the action to return
    bool finished_before_timeout = action_client.waitForResult(ros::Duration(1));
    EXPECT_TRUE(finished_before_timeout);

    actionlib::SimpleClientGoalState state = action_client.getState();
    EXPECT_EQ("SUCCEEDED", state.toString());
}

TEST(SingleGoal, noFinalStateExecuteCallback)
{
    // initialize the action server
    SingleActionServer<actionlib::TestAction> action_server("test_action", &noFinalStateExecuteCallback);

    actionlib::SimpleActionClient<actionlib::TestAction> action_client("test_action", true);

    ros::AsyncSpinner spinner(1);
    spinner.start();

    // wait up to 2 seconds for the action_client server
    bool server_exists = action_client.waitForServer(ros::Duration(2));
    ASSERT_TRUE(server_exists);

    actionlib::TestGoal goal;
    goal.goal = 42; // The answer to life, the universe, and everything
    action_client.sendGoal(goal);

    // wait for the action to return
    bool finished_before_timeout = action_client.waitForResult(ros::Duration(1));
    EXPECT_TRUE(finished_before_timeout);

    actionlib::SimpleClientGoalState state = action_client.getState();
    EXPECT_EQ("ABORTED", state.toString());
    
}

TEST(SingleGoal, destructServerWhileExecuting)
{
    boost::shared_ptr<SingleActionServer<actionlib::TestAction> > action_server(new SingleActionServer<actionlib::TestAction>("test_action",boost::bind(executeCallback, 10, _1, _2)));

    actionlib::SimpleActionClient<actionlib::TestAction> action_client("test_action", true);

    ros::AsyncSpinner spinner(1);
    spinner.start();

    bool server_exists = action_client.waitForServer(ros::Duration(1));
    ASSERT_TRUE(server_exists);

    actionlib::TestGoal goal;
    goal.goal = 42;
    action_client.sendGoal(goal);

    ros::Duration(2).sleep();

    // make sure the goal is active
    actionlib::SimpleClientGoalState goal_state = action_client.getState();
    ASSERT_EQ("ACTIVE", goal_state.toString());

    // destroy the action server
    // which should cause the ok method to start return false
    action_server.reset();

    // I expect this to return instantly
    bool finished_before_timeout = action_client.waitForResult(ros::Duration(1));
    EXPECT_TRUE(finished_before_timeout);

    goal_state = action_client.getState();
    EXPECT_EQ("ABORTED", goal_state.toString());
}

TEST(SingleGoal, cancelCancellableServer)
{
    // create a cancellable action server
    SingleActionServer<actionlib::TestAction> action_server("test_action",
                                                            boost::bind(executeCallback, 10, _1, _2),
                                                            true);

    actionlib::SimpleActionClient<actionlib::TestAction> action_client("test_action", true);

    ros::AsyncSpinner spinner(1);
    spinner.start();

    bool server_exists = action_client.waitForServer(ros::Duration(1));
    ASSERT_TRUE(server_exists);

    actionlib::TestGoal goal;
    goal.goal = 42;
    action_client.sendGoal(goal);

    ros::Duration(2).sleep();

    // make sure the goal is active
    actionlib::SimpleClientGoalState goal_state = action_client.getState();
    ASSERT_EQ("ACTIVE", goal_state.toString());

    // attempt to cancel the action
    action_client.cancelGoal();

    // I expect this to return instantly
    bool finished_before_timeout = action_client.waitForResult(ros::Duration(1));
    EXPECT_TRUE(finished_before_timeout);

    goal_state = action_client.getState();
    EXPECT_EQ("ABORTED", goal_state.toString());
}

TEST(SingleGoal, checkIfGoalAfterACancellationIsExecuted)
{
    // create a cancellable action server
    SingleActionServer<actionlib::TestAction> action_server("test_action",
                                                            boost::bind(executeCallback, 5, _1, _2),
                                                            true);

    actionlib::SimpleActionClient<actionlib::TestAction> action_client("test_action", true);

    ros::AsyncSpinner spinner(1);
    spinner.start();

    bool server_exists = action_client.waitForServer(ros::Duration(1));
    ASSERT_TRUE(server_exists);

    actionlib::TestGoal goal;
    goal.goal = 42;
    action_client.sendGoal(goal);

    ros::Duration(2).sleep();

    // make sure the goal is active
    actionlib::SimpleClientGoalState goal_state = action_client.getState();
    ASSERT_EQ("ACTIVE", goal_state.toString());

    // attempt to cancel the action
    action_client.cancelGoal();

    // I expect this to return instantly
    bool finished_before_timeout = action_client.waitForResult(ros::Duration(1));
    EXPECT_TRUE(finished_before_timeout);

    goal_state = action_client.getState();
    EXPECT_EQ("ABORTED", goal_state.toString());

    // now send a new goal
    action_client.sendGoal(goal);

    finished_before_timeout = action_client.waitForResult(ros::Duration(6));
    EXPECT_TRUE(finished_before_timeout);

    goal_state = action_client.getState();
    EXPECT_EQ("SUCCEEDED", goal_state.toString());
}

TEST(SingleGoal, cancelUncancellableServer)
{
    // create a cancellable action server
    SingleActionServer<actionlib::TestAction> action_server("test_action",
                                                            boost::bind(executeCallback, 4, _1, _2),
                                                            false);

    actionlib::SimpleActionClient<actionlib::TestAction> action_client("test_action", true);

    ros::AsyncSpinner spinner(1);
    spinner.start();

    bool server_exists = action_client.waitForServer(ros::Duration(1));
    ASSERT_TRUE(server_exists);

    actionlib::TestGoal goal;
    goal.goal = 42;
    action_client.sendGoal(goal);

    ros::Duration(1).sleep();

    // make sure the goal is active
    actionlib::SimpleClientGoalState goal_state = action_client.getState();
    ASSERT_EQ("ACTIVE", goal_state.toString());

    // attempt to cancel the action
    action_client.cancelGoal();

    bool finished_before_timeout = action_client.waitForResult(ros::Duration(5));
    EXPECT_TRUE(finished_before_timeout);

    goal_state = action_client.getState();
    EXPECT_EQ("SUCCEEDED", goal_state.toString());
}

TEST(SingleGoal, rejectGoalCallback)
{
    SingleActionServer<actionlib::TestAction> action_server("test_action",
                                                            boost::bind(goalCallback, false, _1),
                                                            boost::bind(executeCallback, .1, _1, _2),
                                                            false);

    actionlib::SimpleActionClient<actionlib::TestAction> action_client("test_action", true);

    ros::AsyncSpinner spinner(1);
    spinner.start();

    bool server_exists = action_client.waitForServer(ros::Duration(1));
    ASSERT_TRUE(server_exists);

    actionlib::TestGoal goal;
    goal.goal = 42;
    action_client.sendGoal(goal);

    ros::Duration(1).sleep();

    bool finished_before_timeout = action_client.waitForResult(ros::Duration(5));
    EXPECT_TRUE(finished_before_timeout);

    actionlib::SimpleClientGoalState goal_state = action_client.getState();
    EXPECT_EQ("REJECTED", goal_state.toString());
}

TEST(MultipleGoals, SecondGoalRejected)
{
    // initialize the action server
    SingleActionServer<actionlib::TestAction> action_server("test_action",
                                                            boost::bind(executeCallback, 4, _1, _2));

    actionlib::SimpleActionClient<actionlib::TestAction> accepted_action_client("test_action", true);
    actionlib::SimpleActionClient<actionlib::TestAction> rejected_action_client("test_action", true);

    ros::AsyncSpinner spinner(1);
    spinner.start();

    // wait up to 2 seconds for the action_server
    bool server_exists = accepted_action_client.waitForServer(ros::Duration(1));
    ASSERT_TRUE(server_exists);

    actionlib::TestGoal goal;
    goal.goal = 42;
    accepted_action_client.sendGoal(goal);

    // wait a few seconds to make the the server
    // has already accepted the first client's goal
    ros::Duration(1).sleep();

    rejected_action_client.sendGoal(goal);

    bool finished_before_timeout = accepted_action_client.waitForResult(ros::Duration(5));
    EXPECT_TRUE(finished_before_timeout);

    actionlib::SimpleClientGoalState accepted_state = accepted_action_client.getState();
    EXPECT_EQ("SUCCEEDED", accepted_state.toString());

    actionlib::SimpleClientGoalState rejected_state = rejected_action_client.getState();
    EXPECT_EQ("REJECTED", rejected_state.toString());
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "single_action_server_test");
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
