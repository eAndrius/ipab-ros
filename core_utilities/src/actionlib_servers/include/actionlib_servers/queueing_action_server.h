#ifndef ACTIONLIB_SERVERS_QUEUEING_ACTION_SERVER_H_
#define ACTIONLIB_SERVERS_QUEUEING_ACTION_SERVER_H_

#include <ros/ros.h>
#include <actionlib_servers/action_server_interface.h>
#include <actionlib/server/action_server.h>
#include <actionlib/action_definition.h>
#include <string>
#include <queue>
#include <set>

#include <boost/function.hpp>
#include <boost/thread.hpp>
#include <boost/thread/condition.hpp>
#include <boost/thread/mutex.hpp>

namespace actionlib_servers
{
    /**
       \brief An Action Server that queues goals for execution in FIFO order
       
       The server will only execute one goal at a time, however, it will queue up
       other goals it receives while executing and then execute them in a FIFO order
       
       Please note that in order to prevent timeouts the goals are accepted when they are placed
       in the queue. This means that even if a goal is returning a state of active it may not actually
       be running.

       Goals in the queue can be cancelled at any time. However, it is up to the user of this class
       whether currently executing goals can be cancelled.
    */
    template<class ActionSpec>
    class QueueingActionServer : public ActionServerInterface<ActionSpec>
    {
    public:
        ACTION_DEFINITION(ActionSpec);

        typedef typename actionlib::ActionServer<ActionSpec>::GoalHandle GoalHandle;
        typedef typename ActionServerInterface<ActionSpec>::GoalCallback GoalCallback;
        typedef typename ActionServerInterface<ActionSpec>::ExecuteCallback ExecuteCallback;

        /**
           \brief Instantiate an instance of a QueueingActionServer

           The constructor makes a QueueingActionServer that will by default accept up to 10
           queued goals before rejecting. However, this number can be increased by changing the max_queue_size
           parameter.

           Goals are only accepted into the queue if the queue has space left (less than max_queue_size elements
           are currently in queue) and the goal_callback returns true. Otherwise the goal is rejected

           \param[in] action_name The name of the action server. Think of this like a topic name
           \param[in] goal_callback The name of the callback function that returns True when a new goal should be accepted and False when it should be rejected
           \param[in] execute_callback The name of the callback function that is run for each goal. It must set a final state on the goal
           \param[in] max_queue_size The maximum number of goals that may be in the queue at any time
           \param[in] can_cancel If true then when a cancel request for an executing goal is received the ok() method will begin returning False. Defaults to false
           
        */
        QueueingActionServer(std::string action_name,
                             GoalCallback goal_callback,
                             ExecuteCallback execute_callback,
                             int max_queue_size=10,
                             bool can_cancel=false);
        /**
           \brief Instantiate an instance of a QueueingActionServer

           The constructor makes a QueueingActionServer that will by default accept up to 10
           queued goals before rejecting. However, this number can be increased by changing the max_queue_size
           parameter.

           Goals are only accepted into the queue if the queue has space left (less than max_queue_size elements
           are currently in queue).

           \param[in] action_name The name of the action server. Think of this like a topic name
           \param[in] execute_callback The name of the callback function that is run for each goal. It must set a final state on the goal
           \param[in] max_queue_size The maximum number of goals that may be in the queue at any time
           \param[in] can_cancel If true then when a cancel request for an executing goal is received the ok() method will begin returning False. Defaults to false
           
        */        
        QueueingActionServer(std::string action_name,
                             ExecuteCallback execute_callback,
                             int max_queue_size=10,
                             bool can_cancel=false);

        /**
           \brief Waits for execute thread to finish before returning
         */
        virtual ~QueueingActionServer();

       /**
           \brief Sets the current goal to SUCCEEDED
           
           Returns a blank result and no message
        */
        virtual void setSucceeded();
        /**
           \brief Sets the current goal to SUCCEEDED

           Returns a blank message and the specified result
         */        
        virtual void setSucceeded(const Result& result);
        /**
           \brief Sets the current goal to SUCCEEDED

           Returns the specified result and message
         */        
        virtual void setSucceeded(const Result& result, const std::string& text);

       /**
           \brief Sets the current goal to ABORTED

           Returns a blank result and no message
         */        
        virtual void setAborted();
        /**
           \brief Sets the current goal to ABORTED

           Returns the specified result and no message
         */        
        virtual void setAborted(const Result& result);
        /**
           \brief Sets the current goal to ABORTED

           Returns the specified result and message
         */       
        virtual void setAborted(const Result& result, const std::string& text);

       /**
           \brief Returns True when its safe to keep executing

           Think of this like calling ros::ok() in a while loop. The execute
           callback should check this function inside of any loops.

           It will return false if the action server instance is being destroyed.
           If the action server is cancellable and the current goal is cancelled
           then ok will also start returning false
         */
        virtual bool ok();
    private:

        /**
           \brief Struct used as comparison function in the Goal Set
         */
        struct GoalHandleCompare : std::binary_function<GoalHandle, GoalHandle, bool>
        {
            bool operator()(const GoalHandle& l, const GoalHandle& r) const
            {
                std::string l_text = l.getGoalID().id;
                std::string r_text = r.getGoalID().id;

                return (l_text.compare(r_text) < 0);
            }
        };
        
        ros::NodeHandle nh_; //!< ROS NodeHandle. Starts ROS if it is not already started in the main program
        actionlib::ActionServer<ActionSpec> action_server_; //!< This is the action server object that does the work
        std::string action_name_; //!< Just here for convenience in case you have multiple servers and need their names

        GoalCallback goal_callback_; //!< Callback function called to determine whether to accept or reject a goal. If NULL then always accepts
        ExecuteCallback execute_callback_; //!< Callback function that executs each goal

        boost::mutex has_active_goal_mutex_; //!< Mutex for has_active_goal_
        bool has_active_goal_; //!< If true then current_goal_ is set to a valid goal that has not been finished

        // the goal currently being executed
        boost::recursive_mutex current_goal_mutex_; //!< Prevents execute thread and main action server thread from both accessing the current_goal_ at the same time
        GoalHandle current_goal_; //!< The handle for the goal currently being executed

        // the queue of goals to be executed
        boost::mutex goal_queue_mutex_; //!< Mutex for goal_queue_
        std::queue<GoalHandle> goal_queue_; //!< Queue of goals in order they were received

        const int max_queue_size_; //!< Specifies how many goals can be waiting in the queue before new ones are rejected

        // If a goal is in the set when
        // taken off of the queue it is executed
        // otherwise it is assumed it has been recalled
        boost::mutex goal_set_mutex_; //!< Mutex for goal_set_
        std::set<GoalHandle, QueueingActionServer<ActionSpec>::GoalHandleCompare> goal_set_; //!< Used to efficiently find and cancel goals in the queue

        boost::thread* execute_thread_; //!< The thread that calls the execute_callback_

        boost::mutex terminate_mutex_; //<! Mutex for need_to_terminate_
        bool need_to_terminate_;  //!< Signals to the execution thread that the action server is trying to shutdown

        const bool can_cancel_; //!< If true a cancel request for a currently executing goal will cause the ok() method to begin returning false

        boost::mutex cancel_mutex_; //!< Mutex for need_to_cancel_
        bool need_to_cancel_; //!< Signals to the execution thread that the current goal has been cancelled

        void goalCallback(GoalHandle handle); //!< decides whether to accept or reject a goal

        void cancelCallback(GoalHandle handle); //!< decides whether to cancel a goal or not

        void shutdown_execute_thread(); //!< Handles clean up of execution thread
        void executeLoop(); //!< This is what is run on the execution thread

    };
}

#include <actionlib_servers/queueing_action_server_imp.h>

#endif
