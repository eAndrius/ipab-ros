#ifndef ACTIONLIB_SERVERS_QUEUEING_ACTION_SERVER_IMP_H_
#define ACTIONLIB_SERVERS_QUEUEING_ACTION_SERVER_IMP_H_

namespace actionlib_servers
{

    template<class ActionSpec>
    QueueingActionServer<ActionSpec>::QueueingActionServer(std::string action_name,
                                                           GoalCallback goal_callback,
                                                           ExecuteCallback execute_callback,
                                                           int max_queue_size,
                                                           bool can_cancel):
        action_server_(nh_,
                       action_name,
                       boost::bind(&QueueingActionServer<ActionSpec>::goalCallback, this, _1),
                       boost::bind(&QueueingActionServer<ActionSpec>::cancelCallback, this, _1),
                       false),
        action_name_(action_name),
        goal_callback_(goal_callback),
        execute_callback_(execute_callback),
        has_active_goal_(false),
        max_queue_size_(max_queue_size),
        need_to_terminate_(false),
        can_cancel_(can_cancel),
        need_to_cancel_(false)
    {
        action_server_.start();
        if(execute_callback_ != NULL)
        {
            execute_thread_ = new boost::thread(boost::bind(&QueueingActionServer<ActionSpec>::executeLoop, this));
        }
        else
        {
            ROS_ERROR("No execution callback provided. Behavior in this case is undefined");
        }
    }
    
    template<class ActionSpec>
    QueueingActionServer<ActionSpec>::QueueingActionServer(std::string action_name,
                                                           ExecuteCallback execute_callback,
                                                           int max_queue_size,
                                                           bool can_cancel) :
        action_server_(nh_,
                       action_name,
                       boost::bind(&QueueingActionServer<ActionSpec>::goalCallback, this, _1),
                       boost::bind(&QueueingActionServer<ActionSpec>::cancelCallback, this, _1),
                       false),
        action_name_(action_name),
        goal_callback_(NULL),
        execute_callback_(execute_callback),
        has_active_goal_(false),
        max_queue_size_(max_queue_size),
        need_to_terminate_(false),
        can_cancel_(can_cancel),
        need_to_cancel_(false)
    {
        action_server_.start();
        if(execute_callback_ != NULL)
        {
            execute_thread_ = new boost::thread(boost::bind(&QueueingActionServer<ActionSpec>::executeLoop, this));
        }
        else
        {
            ROS_ERROR("No execution callback provided. Behavior in this case is undefined");
        }
    }

    template<class ActionSpec>
    QueueingActionServer<ActionSpec>::~QueueingActionServer()
    {
        ROS_DEBUG("Destroying QueueingActionServer");
        if(execute_thread_)
            shutdown_execute_thread();
    }

    template<class ActionSpec>
    void QueueingActionServer<ActionSpec>::shutdown_execute_thread()
    {
        if(execute_callback_)
        {
            {
                boost::mutex::scoped_lock terminate_lock(terminate_mutex_);
                need_to_terminate_ = true;
            }
            ROS_DEBUG("Joining with the execute_thread");
            execute_thread_->join();
            delete execute_thread_;
            execute_thread_ = NULL;
        }
    }

    template<class ActionSpec>
    void QueueingActionServer<ActionSpec>::executeLoop()
    {
        ROS_DEBUG("Starting executeLoop");
        ros::Rate loop_rate(100);
        // only run while the nodehandle says ros is still running
        while(nh_.ok())
        {

            // make sure the server isn't trying to shutdown
            {
                bool need_to_terminate;
                {
                    boost::mutex::scoped_lock terminate_lock(terminate_mutex_);
                    need_to_terminate = need_to_terminate_; // I'll be back!
                }
                if(need_to_terminate)
                {
                    ROS_DEBUG("Terminating the executeLoop");
                    boost::mutex::scoped_lock goal_queue_lock(goal_queue_mutex_);
                    boost::mutex::scoped_lock goal_set_lock(goal_set_mutex_);
                    while(!goal_queue_.empty())
                    {
                        GoalHandle current_handle = goal_queue_.front();
                        goal_queue_.pop();

                        if(goal_set_.find(current_handle) == goal_set_.end())
                        {
                            ROS_DEBUG("Next goal was already recalled");
                            continue;
                        }

                        current_handle.setAborted(Result(), "Action server is shutting down");
                    }
                    
                    break;
                }
            }

            boost::mutex::scoped_lock goal_queue_lock(goal_queue_mutex_);
            boost::mutex::scoped_lock goal_set_lock(goal_set_mutex_);
            if(!goal_queue_.empty())
            {
                typename std::set<GoalHandle>::iterator goal_set_iter;
                {
                    boost::recursive_mutex::scoped_lock current_goal_lock(current_goal_mutex_);
                    current_goal_ = goal_queue_.front();
                    goal_queue_.pop();

                    goal_set_iter = goal_set_.find(current_goal_);
                }
                
                if(goal_set_iter == goal_set_.end())
                {
                    ROS_DEBUG("Next goal was already recalled");
                    continue;
                }
                else
                    goal_set_.erase(goal_set_iter);

                goal_queue_lock.unlock();
                goal_set_lock.unlock();

                {
                    boost::mutex::scoped_lock has_active_goal_lock(has_active_goal_mutex_);
                    has_active_goal_ = true;
                }
                
                ROS_DEBUG("Have an active goal. Executing its callback");
                execute_callback_(this, current_goal_.getGoal());

                ROS_DEBUG("Finished execute callback");
                bool has_active_goal;
                {
                    boost::mutex::scoped_lock has_active_goal_lock(has_active_goal_mutex_);
                    has_active_goal = has_active_goal_;
                }

                if(has_active_goal)
                {
                    ROS_ERROR("Your execute callback did not mark the goal as either succeeded or aborted.\n"
                              "This is an error in your callback. Please fix it before running this server again\n"
                              "This goal has been marked aborted in case your function exited prematurely.\n");
                    setAborted(Result(), "Error in server's execution callback. Please contact the maintainer of this action server to have this bug fixed");
                }
            }
            else
            {
                ROS_DEBUG("Waiting for notification to start execute callback");
                goal_queue_lock.unlock();
                goal_set_lock.unlock();
                loop_rate.sleep();
            }    
        }
    }

    template<class ActionSpec>
    void QueueingActionServer<ActionSpec>::setSucceeded()
    {
        setSucceeded(Result(), "");
    }

    template<class ActionSpec>
    void QueueingActionServer<ActionSpec>::setSucceeded(const Result& result)
    {
        setSucceeded(result, "");
    }

    template<class ActionSpec>
    void QueueingActionServer<ActionSpec>::setSucceeded(const Result& result, const std::string& text)
    {
        ROS_DEBUG("Goal was a success");
        boost::recursive_mutex::scoped_lock current_goal_lock(current_goal_mutex_);
        boost::mutex::scoped_lock has_active_goal_lock(has_active_goal_mutex_);
        boost::mutex::scoped_lock cancel_lock(cancel_mutex_);
        
        if(has_active_goal_)
            current_goal_.setSucceeded(result, text);

        need_to_cancel_ = false;
        has_active_goal_ = false;
    }

    template<class ActionSpec>
    void QueueingActionServer<ActionSpec>::setAborted()
    {
        setAborted(Result(), "");
    }

    template<class ActionSpec>
    void QueueingActionServer<ActionSpec>::setAborted(const Result& result)
    {
        setAborted(result, "");
    }

    template<class ActionSpec>
    void QueueingActionServer<ActionSpec>::setAborted(const Result& result, const std::string& text)
    {
        ROS_DEBUG("Goal was aborted");
        boost::recursive_mutex::scoped_lock current_goal_lock(current_goal_mutex_);
        boost::mutex::scoped_lock has_active_goal_lock(has_active_goal_mutex_);
        boost::mutex::scoped_lock cancel_lock(cancel_mutex_);
        
        if(has_active_goal_)
            current_goal_.setAborted(result, text);

        need_to_cancel_ = false;        
        has_active_goal_ = false;
    }

    template<class ActionSpec>
    bool QueueingActionServer<ActionSpec>::ok()
    {
        boost::mutex::scoped_lock terminate_lock(terminate_mutex_);
        boost::mutex::scoped_lock cancel_lock(cancel_mutex_);
        return !(need_to_terminate_ || need_to_cancel_);
    }

    template<class ActionSpec>
    void QueueingActionServer<ActionSpec>::goalCallback(GoalHandle handle)
    {
        ROS_DEBUG("Received a new goal request");

        int goal_queue_size;
        {
            boost::mutex::scoped_lock goal_queue_lock(goal_queue_mutex_);
            goal_queue_size = (int)goal_queue_.size();
        }

        if(goal_queue_size >= max_queue_size_)
        {
            ROS_DEBUG("Goal was rejected. Queue is full");
            handle.setRejected(Result(), "Server queue is full. Please wait and submit again");
            return;
        }

        bool goal_accepted = true;
        if(goal_callback_)
            goal_accepted = goal_callback_(handle.getGoal());

        if(!goal_accepted)
        {
            ROS_DEBUG("Goal was rejected by user callback");
            handle.setRejected(Result(), "Goal was rejected by user callback");
            return;
        }

        ROS_DEBUG("Goal was accepted");
        {
            boost::mutex::scoped_lock goal_queue_lock(goal_queue_mutex_);
            boost::mutex::scoped_lock goal_set_lock(goal_set_mutex_);
            
            goal_queue_.push(handle);
            goal_set_.insert(handle);
        }

        handle.setAccepted("Added to the goal queue");
    }

    template<class ActionSpec>
    void QueueingActionServer<ActionSpec>::cancelCallback(GoalHandle handle)
    {
        ROS_DEBUG("Received a new cancel request");


        // see if this goal is in the queue
        // if so remove it from the set
        bool goal_in_queue;
        {
            boost::mutex::scoped_lock goal_set_lock(goal_set_mutex_);
            // if its in the set it should be in the queue
            goal_in_queue = (goal_set_.find(handle) != goal_set_.end());
        }
        if(goal_in_queue)
        {
            typename std::set<GoalHandle>::iterator temp_handle = goal_set_.find(handle);
            ROS_DEBUG("Found goal in queue. Cancelling it");
            {
                boost::mutex::scoped_lock goal_set_lock(goal_set_mutex_);
                handle.setAborted(Result(), "Goal was cancelled");
                goal_set_.erase(handle);
            }
        }
        else
        {
            ROS_DEBUG("Goal not found in queue. Assuming its the active goal");
            // otherwise assume its the active goal and cancel it
            {
                boost::mutex::scoped_lock has_active_goal_lock(has_active_goal_mutex_);
                if(has_active_goal_ && can_cancel_)
                {
                    boost::mutex::scoped_lock cancel_lock(cancel_mutex_);
                    need_to_cancel_ = true;
                }
            }
        }

    }
}

#endif
