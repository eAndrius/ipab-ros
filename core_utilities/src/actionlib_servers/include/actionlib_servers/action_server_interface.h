#ifndef ACTIONLIB_SERVERS_ACTION_SERVER_INTERFACE_H_
#define ACTIONLIB_SERVERS_ACTION_SERVER_INTERFACE_H_

#include <actionlib/action_definition.h>
#include <string>
#include <boost/function.hpp>

namespace actionlib_servers
{
    /**
       \brief Abstract Base Class for Action Servers in this package

       Specifies the interface that all action servers in this behavior
       conform to. Because they all conform to this interface each action server
       should be interchangeable with no modificatoins to the executeCallback

       \tparam ActionSpec The action specification generated by .action files in the build step. (e.g. actionlib::TestAction)
     */
    template<class ActionSpec>
    class ActionServerInterface
    {
    public:
        ACTION_DEFINITION(ActionSpec);

        /** @typedef The GoalCallback is run before a goal is accepted. True means accept and False means reject */
        typedef boost::function<bool (const GoalConstPtr&)> GoalCallback;
        /** @typedef The ExecuteCallback is run when a new goal is accepted. It must set a final goal state */
        typedef boost::function<void (ActionServerInterface<ActionSpec>*, const GoalConstPtr&)> ExecuteCallback;
        
        virtual ~ActionServerInterface()
        {}

        /**
           \brief Sets the current goal to SUCCEEDED

           Returns a blank result and no message
         */
        virtual void setSucceeded() = 0;
        /**
           \brief Sets the current goal to SUCCEEDED

           Returns a blank message and the specified result
         */
        virtual void setSucceeded(const Result& result) = 0;
        /**
           \brief Sets the current goal to SUCCEEDED

           Returns the specified result and message
         */
        virtual void setSucceeded(const Result& result, const std::string& text) = 0;

        /**
           \brief Sets the current goal to ABORTED

           Returns a blank result and no message
         */
        virtual void setAborted() = 0;
        /**
           \brief Sets the current goal to ABORTED

           Returns the specified result and no message
         */
        virtual void setAborted(const Result& result) = 0;
        /**
           \brief Sets the current goal to ABORTED

           Returns the specified result and message
         */
        virtual void setAborted(const Result& result, const std::string& text) = 0;

        /**
           \brief Returns True when its safe to keep executing

           Think of this like calling ros::ok() in a while loop. It will return false
           if the action server instance is being destroyed. If the action server is cancellable
           and the current goal is cancelled then ok will also start returning false
         */
        virtual bool ok() = 0;
    };
}

#endif
