cmake_minimum_required(VERSION 2.8.3)
project(exotensions)


set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")
find_package(catkin REQUIRED COMPONENTS
  exotica
)

## System dependencies are found with CMake's conventions
find_package(Boost REQUIRED COMPONENTS signals)
find_package(orocos_kdl REQUIRED)
find_package(EIGEN REQUIRED)

catkin_package(
 LIBRARIES ${PROJECT_NAME}
 CATKIN_DEPENDS exotica
 DEPENDS eigen system_lib boost orocos_kdl
)

include_directories(
  SYSTEM
  ${catkin_INCLUDE_DIRS}
  ${EIGEN_INCLUDE_DIRS}
  ${Boost_INCLUDE_DIRS}
  ${orocos_kdl_INCLUDE_DIRS}
)
