//sin_test_generator:
// create sine-wave test waveforms compatible w/  servo test program


#include <ros/ros.h>
#include <atlas_conversions/joint_names.h>
#include <hku_msgs/BehaviorRegData.h>
#include "lljc_testing_class.h"

int main(int argc, char** argv) 
{
    if(argc < 2)
    {
        ROS_INFO("Usage:\n\t test_generator <test_name>\n\t test_name : std::string\n\t must be a unique name");
        return 1;
    }
    ros::init(argc, argv, argv[1]);

    ros::NodeHandle nh;
    ros::NodeHandle priv_nh("~");

    hku_msgs::BehaviorRegData reg_data;
    reg_data.behavior_name = argv[1];
    reg_data.type = 1;
    reg_data.use_grav_compensation = true;

    for (int i =0; i < NJoints; i++)
    {
        reg_data.pos_select[i] = true;
        reg_data.vel_select[i] = true;
        reg_data.effort_select[i] = true;       
    }

    LLJCTests lljctests(&nh, &priv_nh, &reg_data);

    return lljctests.run();  
}
  
  

       