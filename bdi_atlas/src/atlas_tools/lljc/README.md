# Low Level Joint Controller (LLJC)

This package contains the LLJC, numerous tools to facilitate communication with the LLJC, and code for gain testing.  The new LLJC is fully compatible with legacy code, but numerous features have been deprecated.

## New Features

To eliminate the possibiliy of multiple controllers/behaviors issuing commands to the LLJC and consequently to Atlas, it was necessary to create and enforce registration and switching behaviors for the LLJC.  These behaviors ensured only one controller/behavior could be actively issuing commands to Atlas, regardless of the number of controllers/behaviors issuing commands to the LLJC.  The new features were driven by this necessity.

### Registration

Before a controller/behavior can issue commands to the LLJC, it must first register with the LLJC to receive a topic that the LLJC will actively monitor for commands. The registration feature is fully encapsulated by the functions in the `register_with_LLJC.h` file.  It is located in `/low_level_joint_controller/include/`.  Once registered, the controller/behavior must assign the topic returned from the registration function call to the corresponding command publisher.  See `/low_level_joint_controller/tests/src/lljc_testing_class.h` for an example implementation.

### Switch

When the user wants the LLJC to start sending a behavior's commands to Atlas, the user must first switch to tat behavior.  This ensures that only one controller is sending commands to Atlas at any given time.  To switch, use the scripts provided in `/low_level_joint_controller/switch_behavior_scripts/` .  If their is no script for our behavior, use the following command: `rosservice call /lljc/switch_behavior <index of your behavior>`  See the Available Behaviors section to determine the index of your behavior.

### Command Limits

In order to make transitions between behaviors safe and relatively smooth, the LLJC enforces limits on the difference between the current joint angles and the commanded joint angles.  For example, if the left elbow is currenty at 0.2 radians and a controller issued a command to move it to 1.3 radians, the lljc would determine this is too big of a difference and the command would be ignored.  These limits are still being tested, and are subject to change. 

### Available Behaviors

The LLJC publishes a list of the available behaviors currently registered with the LLJC on the topic `/lljc/behavior_list`.  These behaviors can be switched to at any time.  The behavior index corresponds to the order in which they are listed.  For example, the first behavior listed corresponds to an index of 0. The nth behavior listed corresponds to an index of n-1.

### Current Behavior

The LLJC also publishes the behavior whose commands are currently being sent to Atlas on the topic `/lljc/current_behavior` .  This is updated whenever the user switches to a new behavior.

## Deprecated Features

Several features no longer provide desirable behavior, and have since been deprecated.

### Set K_Effort Service

The real Atlas doesn't use the `k_effort` field, and it makes more sense to set the `k_effort` in each command sent to the LLJC.

### Set Controller Gains

Changing the gains on-the-fly is a very dangerous thing to do.  On top of that, the gains that are being used were found after meticulous testing, and are known to be relatively stable (*shakes fist at the shoulder_usy joints*).

### Set Controller Efforts

It makes more sense to send efforts with the command msgs, rather than send them separately.

### LLJC ON/OFF Service

This service has been deprecated and replaced with an internal halt behavior.  It can be switched to using the `switch_halt` script located in `/low_level_joint_controller/switch_behavior_scripts/` .  The halt behavior ignores all commands sent to the LLJC and does not send any new commands to Atlas.  It only keeps track of the sensor data if the joint is under BDI control, or the last issued command if the joint is under user control.

### Report Command Service

This service is deprecated, but still functions normally.  Taking its place is the `/lljc/last_cmd/` topic, which publishes the last received command published by the current behavior.

## FAQ

### How Do I launch the LLJC?

Just run the top-level launch file and it will launch the lljc along with everything else it needs.

If you are doing task 3 on the simulator:

    roslaunch fc_bringup sim_task_3.launch
    roslaunch ocs_bringup sim_task_3.launch

If you are doing task 2 with the real robot:

    roslaunch fc_bringup trials_task_2.launch
    roslaunch ocs_bringup trials_task_2.launch

### I am sending commands to the LLJC, but it isn't responding!

Did you register your behavior?
Check to see if it is registered with the followin command: `rostopic echo /lljc/behavior_list`
If your behavior isn't listed, you need to register.  See the Registration section above.

Did you check that the current behavior is indeed your behavior?
Check with the following command: `rostopic echo /lljc/current_behavior/`
If it isn't you need to switch to your behavior. See below.

Did you switch to the proper behavior? 
Switch with the following command: `rosservice call /lljc/switch_behavior <index of your behavior>`
See the Available Behaviors section to determine the index of your behavior.

Are your commands tripping the command limits?
In the terminal you launched the LLJC, check to see if you have `Error: Current command exceeded safe operation` messages scrolling across your screen.  If you do, congratulations, I stopped you from breaking a 2 million USD robot.  You should probably buy me a juice box or something.

## TODO 



