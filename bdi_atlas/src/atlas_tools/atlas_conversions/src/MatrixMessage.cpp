/* 
 * File:   MatrixMessage.h
 * Author: axg254
 *
 * Created on October 30, 2013, 1:01 PM
 */

#include <atlas_conversions/MatrixMessage.h>
#include <Eigen/Eigen>

namespace atlas_conversions {

void matrixMsgToEigen(const hku_msgs::Float64MatrixStamped& msg,  Eigen::MatrixXd& mat){
    mat.resize(msg.rows,msg.cols);
    for(int index=0;index<(int)(mat.rows()*mat.cols());index++){
        *(mat.data() + index)=msg.matrix[index];    
    }
};

void matrixEigenToMsg(const Eigen::MatrixXd& mat, hku_msgs::Float64MatrixStamped& msg){
    msg.rows=mat.rows();
    msg.cols=mat.cols();
    msg.matrix.resize(mat.rows()*mat.cols());
    for(int index=0;index<(int)(mat.rows()*mat.cols());index++){
        msg.matrix[index]=*(mat.data() + index);
    }
};

}

