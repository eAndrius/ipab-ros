/* 
 * File:   MatrixMessage.h
 * Author: axg254
 *
 * Created on October 30, 2013, 1:01 PM
 */

#ifndef MATRIXMESSAGE_H
#define	MATRIXMESSAGE_H

#include <hku_msgs/Float64MatrixStamped.h>
#include <Eigen/Eigen>

namespace atlas_conversions {
void matrixMsgToEigen(const hku_msgs::Float64MatrixStamped& msg,  Eigen::MatrixXd& mat);
void matrixEigenToMsg(const Eigen::MatrixXd& mat, hku_msgs::Float64MatrixStamped& msg);
}

#endif	/* MATRIXMESSAGE_H */

