/******************************\
|   Defines the offsets of the |
| grasp frames (l/r) w.r.t. to |
| the palm frames.             |
\******************************/

#include <tf/transform_broadcaster.h>

//!< Define the Frame offset w.r.t. hand frame
tf::Vector3 Right_Grasp_Off = tf::Vector3(0.03, -0.25, 0.045);
tf::Vector3 Left_Grasp_Off = tf::Vector3(0.03, 0.25, 0.045);

tf::Quaternion Right_Grasp_Orient = tf::Quaternion(tf::Vector3(0,0,1), 2.23); //Orientation Angle offset
tf::Quaternion Left_Grasp_Orient = tf::Quaternion(tf::Vector3(0,0,1), 0.91);
