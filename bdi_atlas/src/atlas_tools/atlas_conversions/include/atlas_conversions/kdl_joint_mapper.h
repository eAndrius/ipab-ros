/*
 * kdl_joint_mapper.h
 *
 *  Created on: 11 Oct 2013
 *      Author: s0972326
 */

#ifndef KDL_JOINT_MAPPER_H_
#define KDL_JOINT_MAPPER_H_

#include <kdl/tree.hpp>
#include <vector>

/**
 * \brief This class creates the joint maps for given chain/tree. This class is a singleton.
 */
class JointMapper
{
  private:
    /// \brief instance of the singleton class
    static JointMapper* instance;
  protected:
    /// \brief Private constructor
    JointMapper();
  public:
    /// \brief Virtual destructor
    virtual ~JointMapper();
    /// \brief Instanciates the mapper
    static JointMapper* getInstance();

    /**
    * \brief Creates a joint index map between the chain joints and the internal map of joints
    * \param jointMap_ Returned map
    * \param chain Input chain to compute map for
    * \return 0 if successful, >0 id of the joint that could not be found (+1)
    */
    int GetJointMap(std::vector< int >& jointMap_, KDL::Chain& chain);

    /**
    * \brief Creates a joint index map between the chain joints and the internal map of joints
    * \param jointMap_ Returned map
    * \param chain Input chain to compute map for
    * \return 0 if successful, >0 id of the joint that could not be found (+1)
    */
    int GetJointMap(std::vector< int >& jointMap_, KDL::Tree& tree);

    /**
     * \brief Returns number of joints of the robot
     * \return Number of joints
     */
    int GetNJoints();

};

#endif /* KDL_JOINT_MAPPER_H_ */
