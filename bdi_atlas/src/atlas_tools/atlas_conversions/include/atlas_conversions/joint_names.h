#ifndef JOINT_NAMES_H
#define JOINT_NAMES_H

#include <string>
#include <vector>

#include <ros/ros.h>

// Here we are using the ATLAS_JOINT_NAMES_MAP macro to define both an enum with
// joint indices and an array of joint names as strings. Any joints specified
// here will be added to the AtlasJoints enum and g_atlas_joint_names array in the
// order they are listed. The order below corresponds to the indices used by
// the AtlasCommand message.

/**
 * \brief Joint group enum
 */
enum RobotJointGroup
{
  JointGroupAtlas        = 0x01,
  JointGroupHokuyo       = 0x02,
  JointGroupSandiaHand   = 0x04
};

/// \brief Number of joints and offsets
enum { JointGroupAtlas_NJoints = 28 };
/// \brief Number of joints and offsets (backward compatible)
enum { NJoints = JointGroupAtlas_NJoints };
/// \brief Offset to substract to get Sandia hand indexing from zero
enum { JointGroupSandiaHand_Offset = 28 };

/**
 * This C preprocessor idiom lets us define a list of tokens and reuse them in
 * different contexts. Passing a macro to ATLAS_JOINT_NAMES_MAP will call that
 * macro with each of the joint names as argument, in turn.
 * See http://www.drdobbs.com/the-new-c-x-macros/184401387 for more info.
 * @param F A preprocessor macro to call. F( joint_name, joint_id, model_version ) will be called for
 * each joint name.
 * Joint names should not contain duplicates (e.g. v2 and v3 both have a joint named l_leg_kny).
 * For clarity, all joint names are written down but the duplicates are commented out.
 * When adding new joints, make sure check for duplicate names.
 */
#define ATLAS_JOINT_NAMES_MAP(F) \
/* Version 3 joint names */ \
/* back */\
/* enum value name, index, major version, joint group */\
F( back_bkz , 0 , 3 , JointGroupAtlas )\
F( back_bky , 1 , 3 , JointGroupAtlas )\
F( back_bkx , 2 , 3 , JointGroupAtlas )\
/* neck */\
F( neck_ry , 3 , 3 , JointGroupAtlas )\
/* left leg */\
F( l_leg_hpz , 4 , 3 , JointGroupAtlas )\
F( l_leg_hpx , 5 , 3 , JointGroupAtlas )\
F( l_leg_hpy , 6 , 3 , JointGroupAtlas )\
F( l_leg_kny , 7 , 3 , JointGroupAtlas )\
F( l_leg_aky , 8 , 3 , JointGroupAtlas )\
F( l_leg_akx , 9 , 3 , JointGroupAtlas )\
/* right leg */\
F( r_leg_hpz , 10 , 3 , JointGroupAtlas )\
F( r_leg_hpx , 11 , 3 , JointGroupAtlas )\
F( r_leg_hpy , 12 , 3 , JointGroupAtlas )\
F( r_leg_kny , 13 , 3 , JointGroupAtlas )\
F( r_leg_aky , 14 , 3 , JointGroupAtlas )\
F( r_leg_akx , 15 , 3 , JointGroupAtlas )\
/* left arm */\
F( l_arm_shy , 16 , 3 , JointGroupAtlas )\
F( l_arm_shx , 17 , 3 , JointGroupAtlas )\
F( l_arm_ely , 18 , 3 , JointGroupAtlas )\
F( l_arm_elx , 19 , 3 , JointGroupAtlas )\
F( l_arm_wry , 20 , 3 , JointGroupAtlas )\
F( l_arm_wrx , 21 , 3 , JointGroupAtlas )\
/* right arm */\
F( r_arm_shy , 22 , 3 , JointGroupAtlas )\
F( r_arm_shx , 23 , 3 , JointGroupAtlas )\
F( r_arm_ely , 24 , 3 , JointGroupAtlas )\
F( r_arm_elx , 25 , 3 , JointGroupAtlas )\
F( r_arm_wry , 26 , 3 , JointGroupAtlas )\
F( r_arm_wrx , 27 , 3 , JointGroupAtlas )\
/* Version 2 joint names */ \
/* back */\
F( back_lbz , 0 , 2 , JointGroupAtlas )\
F( back_mby , 1 , 2 , JointGroupAtlas )\
F( back_ubx , 2 , 2 , JointGroupAtlas )\
/* neck */\
F( neck_ay , 3 , 2 , JointGroupAtlas )\
/* left leg */\
F( l_leg_uhz , 4 , 2 , JointGroupAtlas )\
F( l_leg_mhx , 5 , 2 , JointGroupAtlas )\
F( l_leg_lhy , 6 , 2 , JointGroupAtlas )\
/*F( l_leg_kny , 7 , 2 , JointGroupAtlas )*/\
F( l_leg_uay , 8 , 2 , JointGroupAtlas )\
F( l_leg_lax , 9 , 2 , JointGroupAtlas )\
/* right leg */\
F( r_leg_uhz , 10 , 2 , JointGroupAtlas )\
F( r_leg_mhx , 11 , 2 , JointGroupAtlas )\
F( r_leg_lhy , 12 , 2 , JointGroupAtlas )\
/*F( r_leg_kny , 13 , 2 , JointGroupAtlas )*/\
F( r_leg_uay , 14 , 2 , JointGroupAtlas )\
F( r_leg_lax , 15 , 2 , JointGroupAtlas )\
/* left arm */\
F( l_arm_usy , 16 , 2 , JointGroupAtlas )\
/*F( l_arm_shx , 17 , 2 , JointGroupAtlas )*/\
/*F( l_arm_ely , 18 , 2 , JointGroupAtlas )*/\
/*F( l_arm_elx , 19 , 2 , JointGroupAtlas )*/\
F( l_arm_uwy , 20 , 2 , JointGroupAtlas )\
F( l_arm_mwx , 21 , 2 , JointGroupAtlas )\
/* right arm */\
F( r_arm_usy , 22 , 2 , JointGroupAtlas )\
/*F( r_arm_shx , 23 , 2 , JointGroupAtlas )*/\
/*F( r_arm_ely , 24 , 2 , JointGroupAtlas )*/\
/*F( r_arm_elx , 25 , 2 , JointGroupAtlas )*/\
F( r_arm_uwy , 26 , 2 , JointGroupAtlas )\
F( r_arm_mwx , 27 , 2 , JointGroupAtlas )\
/* Left Sandia hand joints */\
F( left_f0_j0 , 28 , 1 , JointGroupSandiaHand )\
F( left_f0_j1 , 29 , 1 , JointGroupSandiaHand )\
F( left_f0_j2 , 30 , 1 , JointGroupSandiaHand )\
F( left_f1_j0 , 31 , 1 , JointGroupSandiaHand )\
F( left_f1_j1 , 32 , 1 , JointGroupSandiaHand )\
F( left_f1_j2 , 33 , 1 , JointGroupSandiaHand )\
F( left_f2_j0 , 34 , 1 , JointGroupSandiaHand )\
F( left_f2_j1 , 35 , 1 , JointGroupSandiaHand )\
F( left_f2_j2 , 36 , 1 , JointGroupSandiaHand )\
F( left_f3_j0 , 37 , 1 , JointGroupSandiaHand )\
F( left_f3_j1 , 38 , 1 , JointGroupSandiaHand )\
F( left_f3_j2 , 39 , 1 , JointGroupSandiaHand )\
/* Right Sandia hand joints */\
F( right_f0_j0 , 40 , 1 , JointGroupSandiaHand )\
F( right_f0_j1 , 41 , 1 , JointGroupSandiaHand )\
F( right_f0_j2 , 42 , 1 , JointGroupSandiaHand )\
F( right_f1_j0 , 43 , 1 , JointGroupSandiaHand )\
F( right_f1_j1 , 44 , 1 , JointGroupSandiaHand )\
F( right_f1_j2 , 45 , 1 , JointGroupSandiaHand )\
F( right_f2_j0 , 46 , 1 , JointGroupSandiaHand )\
F( right_f2_j1 , 47 , 1 , JointGroupSandiaHand )\
F( right_f2_j2 , 48 , 1 , JointGroupSandiaHand )\
F( right_f3_j0 , 49 , 1 , JointGroupSandiaHand )\
F( right_f3_j1 , 50 , 1 , JointGroupSandiaHand )\
F( right_f3_j2 , 51 , 1 , JointGroupSandiaHand )\
F( hokuyo_joint , 52 , 1 , JointGroupHokuyo )

// We create an enum from the above list of joints. The enum value for each
// joint corresponds to the Atlas joint index. 


/**
 * An enumeration of all the Atlas joints. These can be used to index into
 * the joint arrays of the AtlasCommand and AtlasState messages, and the
 * g_atlas_joint_names array.
 */
// TODO move to namespace atlas
enum AtlasJoints
{
/** @cond */
#define ENUM_ENTRY(NAME, ID, VER, GROUP) NAME=ID,
  ATLAS_JOINT_NAMES_MAP(ENUM_ENTRY)
#undef ENUM_ENTRY
/** @endcond */
};

/**
 * \brief joint names for atlas, the hands, and head. In future these should be split up.
 */
namespace atlas {

/**
 * \brief Initializes the joint names vector to a particular version of the model. Call this function at the beginning of your program.
 * By default, latest model version is used.
 * \param version Desired version. -1 returns latest version (default).
 */
// NOTE intentionally didn't implement minor version number, it complicates things and we don't need it right now.
void initJointNames(int version = -1);

/**
 * \brief Returns the joint name for the current Atlas version for the specified joint index, or "ERROR" if bad index.
 * \param idx The joint index.
 */
std::string jointNameFromIdx(int idx);

/**
 * \brief Returns the joint index for the specified joint name, or -1 on error.
 * \param idx The joint index.
 */
int jointIdxFromName(std::string const& name);

/**
 * \brief Returns the total number of robot joints.
 */
int numJoints();

} // namespace atlas


// Legacy support, just in case

ROS_DEPRECATED extern std::vector<const char*> AtlasJointNames;
ROS_DEPRECATED extern std::vector<std::string> AtlasJointNamesStr;
ROS_DEPRECATED extern const char* AtlasJointNamesInternal[];
ROS_DEPRECATED extern int AtlasJointIndicesInternal[];
ROS_DEPRECATED extern double AtlasJointVersionsInternal[];
ROS_DEPRECATED extern RobotJointGroup AtlasJointGroupsInternal[];
ROS_DEPRECATED extern void InitJointNames(double Version);

#endif // JOINT_NAMES_H

