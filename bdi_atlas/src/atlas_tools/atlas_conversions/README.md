# This package provides method for converting values related to Atlas robot
joint_names.h defines enum containing all joint indices, NJoint variable containing number of joints and a vector of string names (AtlasJointNamesStr/AtlasJointNames).
Function InitJointNames(Version, JointGroup) initiales the above variables to specific version and configuration of the robot (the default is the latest version of the 28 atlas joints).

kdl_joint_mapper.h contains a class for mapping joint names to KDL joint indices.

## How to use this
Check out src/example.cpp